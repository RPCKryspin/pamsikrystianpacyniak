#include "AI.h"
#include <cstdlib>

enum Scores {X = 10, O = -10, Tie = 0};

AI::AI(Board& Board, char player_choice) : board_AI{Board}
{
    if('x' == player_choice)
        decision_AI = 'o';
    else
        decision_AI = 'x';
}

void AI::Turn(bool First)
{
    int score;
    int BestMoveX = 0;
    int BestMoveY = 0;
    int BestScore;

    if (!First) //Sprawdza czy gracz wykonuje pierwszy ruch czy SI
    {
        BestScore = INT_MAX;
        for (int i = 0; i < board_AI.GetSize(); i++)
        {
            for (int j = 0; j < board_AI.GetSize(); j++)
            {
                if (' ' == board_AI[i][j].data)
                {
                    board_AI[i][j].data = decision_AI;
		            //score = minimax(board_AI,board_AI.GetSize(), false);
                    score = minimaxAB(board_AI, board_AI.GetSize() ^ 2, -INT_MAX, INT_MAX, false);
                    board_AI[i][j].data = ' ';
                    if (score < BestScore)
                    {
                        BestScore = score;
                        BestMoveX = i;
                        BestMoveY = j;
                    }
                }
            }
        }
    }

    else   //SI==First
    {
        BestScore = -INT_MAX;
        for (int i = 0; i < board_AI.GetSize(); i++)
        {
            for (int j = 0; j < board_AI.GetSize(); j++)
            {
                if (' ' == board_AI[i][j].data)
                {
                    board_AI[i][j].data = decision_AI;
		            //score = minimax(board_AI,board_AI.GetSize(), false);
                    score = minimaxAB(board_AI, board_AI.GetSize() ^ 2, -INT_MAX, INT_MAX, true);
                    board_AI[i][j].data = ' ';
                    if (score > BestScore)
                    {
                        BestScore = score;
                        BestMoveX = i;
                        BestMoveY = j;
                    }
                }
            }
        }
    }
    board_AI[BestMoveX][BestMoveY].Set(decision_AI);
}

int minimax(Board board, int depth, bool Maxing)
{
    int score;
    int result = CheckWinner(board);
    if (result != INT_MAX) return result;

    if (Maxing)
    {
        int MaxEval = -INT_MAX;
        for (int i = 0; i < board.GetSize(); i++)
        {
            for (int j = 0; j < board.GetSize(); j++)
            {
                if (' ' == board[i][j].data)
                {
                    board[i][j].data = 'o';
                    score = minimax(board, depth+1, false);
                    board[i][j].data = ' ';
                    MaxEval = std::max(score, MaxEval);
                }
            }
        }
        return MaxEval;
    }

    else
    {
        int MinEval = INT_MAX;
        for (int i = 0; i < board.GetSize(); i++)
        {
            for (int j = 0; j < board.GetSize(); j++)
            {
                if (' ' == board[i][j].data)
                {
                    board[i][j].data = 'x';
                    score = minimax(board, depth+1, true);
                    board[i][j].data = ' ';
                    MinEval = std::min(score, MinEval);
                }
            }
        }
        return MinEval;
    }
}

int minimaxAB(Board board, int depth,int alpha, int beta, bool Maxing)
{
    int score;
    int result = CheckWinner(board);
    if (result != INT_MAX) return result;

    if (Maxing)
    {
        int MaxEval = -INT_MAX;
        for (int i = 0; i < board.GetSize(); i++)
        {
            for (int j = 0; j < board.GetSize(); j++)
            {
                if (' ' == board[i][j].data)
                {
                    board[i][j].data = 'o';
                    score = minimaxAB(board, depth - 1, alpha, beta, false);
                    board[i][j].data = ' ';
                    MaxEval = std::max(score, MaxEval);
                    alpha = std::max(score, alpha);
                }
                if (beta <= alpha) break;
            }
            if (beta <= alpha) break;
        }
        return MaxEval;
    }

    else
    {
        int MinEval = INT_MAX;
        for (int i = 0; i < board.GetSize(); i++)
        {
            for (int j = 0; j < board.GetSize(); j++)
            {
                if (' ' == board[i][j].data)
                {
                    board[i][j].data = 'x';
                    score = minimaxAB(board, depth - 1, alpha, beta, true);
                    board[i][j].data = ' ';
                    MinEval = std::min(score, MinEval);
                    beta = std::min(score, beta);
                }
                if (beta <= alpha) break;
            }
            if (beta <= alpha) break;
        }
        return MinEval;
    }
}

int CheckWinner(Board& board)
{
    int Xscore = 0;
    int Oscore = 0;

    //Up to Down
    for (int i = 0; i < board.GetSize(); i++)
    {
        for (int j = 0; j < board.GetSize(); j++)
        {
            if (board[i][j].data == 'x') Xscore++;
            if (board[i][j].data == 'o') Oscore++;
        }
        if (Xscore == board.GetSize()) return X;
        if (Oscore == board.GetSize()) return O;
        Xscore = 0;
        Oscore = 0;
    }

    //Left To Right
    for (int i = 0; i < board.GetSize(); i++)
    {
        for (int j = 0; j < board.GetSize(); j++)
        {
            if (board[j][i].data == 'x') Xscore++;
            if (board[j][i].data == 'o') Oscore++;
        }
        if (Xscore == board.GetSize()) return X;
        if (Oscore == board.GetSize()) return O;
        Xscore = 0;
        Oscore = 0;
    }

    //Diagonal Left to Right
    for (int i = 0; i < board.GetSize(); i++)
    {

        if (board[i][i].data == 'x') Xscore++;
        if (board[i][i].data == 'o') Oscore++;
    }
    if (Xscore == board.GetSize()) return X;
    if (Oscore == board.GetSize()) return O;
    Xscore = 0;
    Oscore = 0;

    //Diagonal Right to Left
    for (int j = 0, i = board.GetSize() - 1; -1 < i; i--)
    {
        if (board[i][j].data == 'x') Xscore++;
        if (board[i][j].data == 'o') Oscore++;
        j++;
    }
    if (Xscore == board.GetSize()) return X;
    if (Oscore == board.GetSize()) return O;

    int takenscore = 0;
    for (int i = 0; i < board.GetSize(); i++)
    {
        for (int j = 0; j < board.GetSize(); j++)
        {
            if (board[j][i].taken) takenscore++;
        }
    }
    if (takenscore == board.GetSize() * board.GetSize()) return Tie;
    return INT_MAX;
}