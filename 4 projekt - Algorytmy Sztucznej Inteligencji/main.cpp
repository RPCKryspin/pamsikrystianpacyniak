#include <iostream>
#include "Board.h"
#include "AI.h"
#include "Game.h"

int main()
{
    char answer = 'x';
    int BoardSize;
    std::cout << "Wprowadz rozmiar planszy: ";
    std::cin >> BoardSize;
    while(std::cin.fail() || BoardSize<= 0)
    {
        std::cout << "Rozmiar musi byc liczba dodatnia. Wprowadz ponownie" << std::endl;
        std::cin.clear();
        std::cin.ignore(1000,'\n');
        std::cout << std::endl << "Wybieram rozmiar:  ";
        std::cin >> BoardSize;
    }

    Board GBoard(BoardSize);
    AI Tic_Tac_Toe_AI(GBoard, answer);
    Game Tic_Tac_Toe_Game(GBoard, Tic_Tac_Toe_AI, answer);

    while (true)
    {
        Tic_Tac_Toe_Game.Play();
        std::cout << "\nCzy Chcesz zagrac ponownie ?\n T/N" << std::endl;
        std::cin >> answer;
        if ('N' == answer || 'n' == answer){break;}
        Tic_Tac_Toe_Game.GetBoard().Clean();
    }
    std::cout << "\nKoniec Gry"<< std::endl;
    return 0;
}
