#ifndef AI_PAMSI_AI_H
#define AI_PAMSI_AI_H
#include "Board.h"

class AI    //Klasa Artificial Intelligence - Algorytmy AI
{
private:
    char decision_AI;                                                    //Znak badany i uzywany przez AI
    Board& board_AI;                                                     //Plansza
public:
    AI(Board& Board, char player_choice);                                //Konstruktor parametryczny klasy AI
    ~AI()= default;                                                      //Domyslny Destruktor klasy AI
    void Turn(bool First);                                               //Metoda Badajaca przebieg tury rozgrywki
};
int minimax(Board board, int depth, bool Maxing);                        //Algorytm MiniMax bez modyfikacji
int minimaxAB(Board board, int depth, int alpha, int beta, bool Maxing); //Algorytm MiniMax z alfa-beta cieciami
int CheckWinner(Board& board);                                           //Funkcja sprawdza czy na planszy ktos wygral
#endif //AI_PAMSI_AI_H
