#ifndef AI_PAMSI_BOARD_H
#define AI_PAMSI_BOARD_H
#include <vector>
#include <iostream>
#include "Cell.h"

class Board     //Klasa Plansza - Macierz
{
private:
    int Board_Size;                                         //Rozmiar Planszy
    std::vector<std::vector<Cell>> board;                   //Macierz Komorek
public:
    ~Board()= default;                                      //Destruktor domyslny Planszy
    Board();                                                //Konstruktor Bezparametryczny domyslny Planszy
    Board(int size);                                        //Konstruktor Parametryczny Planszy
    int GetSize() const {return Board_Size;};               //Metoda zwraca rozmiar Planszy
    void Clean();                                           //Metoda czysci Plansze z komorek
    std::vector<Cell> operator[](int index) const;    //Metody dostepu przeciazaja operator indeksujacy
    std::vector<Cell>& operator[](int index);         //jedna zwraca obiekt a druga adres(referencja)
    //Metoda zaprzyjazniona przeciaza operator strumienia co pozwala to na wypisanie Macierzy komorek czyli planszy
    friend std::ostream& operator<<(std::ostream& stream, const Board& board_stream);
};
#endif //AI_PAMSI_BOARD_H
