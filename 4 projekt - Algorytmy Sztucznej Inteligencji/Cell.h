#ifndef AI_PAMSI_CELL_H
#define AI_PAMSI_CELL_H
#include <iostream>

class Cell  //Klasa Komorka(Pole) Planszy
{
public:
    char data = ' ';                                                  //Pole i jego domyslna zawartosc
    bool taken = false;                                               //Bezpiecznik zajetosci pola
    Cell()= default;                                                  //Konstruktor Bezparametryczny
    Cell(char x): data(x){};                                          //Konstruktor Parametryczny
    void Set(char x){data = x;taken = true;}                          //Metoda wstawia w pole znak
    void Free(){data = ' ';taken = false;}                            //Metoda czysci pola komorki ze znakow
    //Metoda zaprzyjazniona przeciaza operator strumienia co pozwala to na wypisanie komorki na konsoli
    friend std::ostream& operator<<(std::ostream& stream, const Cell& cell){stream << cell.data;return stream;}
};
#endif //AI_PAMSI_CELL_H
