#include "Game.h"
#include<random>

Game::Game(Board& board, AI ai, char player_choice) : Game_AI(ai), Game_board(board), Game_player_choice(player_choice)
{
    if (Game_player_choice == 'x')
        AI_Icon = 'o';
    else if (Game_player_choice == 'o')
        AI_Icon = 'x';
    else
        AI_Icon = ' ';
}

int Game::ScanBoard()
{
    int player_score = 0;
    int AI_score = 0;

    //Up to Down
    for (int i = 0; i < Game_board.GetSize(); i++)
    {
        for (int j = 0; j < Game_board.GetSize(); j++)
        {
            if (Game_board[i][j].data == Game_player_choice) player_score++;
            if (Game_board[i][j].data == AI_Icon) AI_score++;
        }
        if (player_score == Game_board.GetSize())
        {
            std::cout << "Human Win" << std::endl;
            return 1;
        }
        if (AI_score == Game_board.GetSize())
        {
            std::cout << "AI Win" << std::endl;
            return -1;
        }
        player_score = 0;
        AI_score = 0;
    }

    //Left To Right
    for (int i = 0; i < Game_board.GetSize(); i++)
    {
        for (int j = 0; j < Game_board.GetSize(); j++)
        {
            if (Game_board[j][i].data == Game_player_choice) player_score++;
            if (Game_board[j][i].data == AI_Icon) AI_score++;
        }
        if (player_score == Game_board.GetSize())
        {
            std::cout << "Human Win" << std::endl;
            return true;
        }
        if (AI_score == Game_board.GetSize())
        {
            std::cout << "AI Win" << std::endl;
            return true;
        }
        player_score = 0;
        AI_score = 0;
    }

    //Diagonal Left to Right
    for (int i = 0; i < Game_board.GetSize(); i++)
    {

        if (Game_board[i][i].data == Game_player_choice) player_score++;
        if (Game_board[i][i].data == AI_Icon) AI_score++;
    }

    if (player_score == Game_board.GetSize())
    {
        std::cout << "Human Win!" << std::endl;
        return true;
    }

    if (AI_score == Game_board.GetSize())
    {
        std::cout << "AI Win!" << std::endl;
        return true;
    }
    player_score = 0;
    AI_score = 0;

    //Diagonal Right to Left
    for (int j = 0, i = Game_board.GetSize()-1; -1 < i ; i--)
    {
        if (Game_board[i][j].data == Game_player_choice) player_score++;
        if (Game_board[i][j].data == AI_Icon) AI_score++;
        j++;
    }

    if (player_score == Game_board.GetSize())
    {
        std::cout << "Human Win" << std::endl;
        return 1;
    }
    else if (AI_score == Game_board.GetSize())
    {
        std::cout << "AI Win" << std::endl;
        return -1;
    }

    //Tie
    int takenscore = 0;
    for (int i = 0; i < Game_board.GetSize(); i++)
    {
        for (int j = 0; j < Game_board.GetSize(); j++)
        {
            if (Game_board[j][i].taken) takenscore++;
        }
    }

    if (takenscore == Game_board.GetSize() * Game_board.GetSize())
    {
        std::cout << "We have a Tie" << std::endl;
        return 0;
    }
    return -10;
}

void Game::Play()
{
    int coord_x = 0;
    int coord_y = 0;
    bool First = false;
    std::cout << "\nYou start with: " << Game_player_choice << std::endl;
    std::cout << Game_board << std::endl;

    if ('o' == Game_player_choice)
    {
        std::random_device rd; // obtain a random number from hardware
        std::mt19937 gen(rd()); // seed the generator
        std::uniform_int_distribution<> distr(0, Game_board.GetSize() - 1); // define the range

        First = true;
        std::cout << "AI's Turn\n";
        Game_board[distr(gen) % Game_board.GetSize()][distr(gen) % Game_board.GetSize()].Set('x');
        std::cout << Game_board << std::endl;
    }

    while (true)
    {
        std::cout << "Your Turn\n";
        std::cout << "Enter the X and Y coordinates\n";
        std::cin >> coord_x >> coord_y;

        if (coord_x > Game_board.GetSize() || coord_x < 0)
        {
            std::cout << "Please Enter different coordinates, those are incorrect\n";
            std::cin >> coord_x >> coord_y;
        }

        if (coord_y > Game_board.GetSize() || coord_y < 0)
        {
            std::cout << "Please Enter different coordinates, those are incorrect\n";
            std::cin >> coord_x >> coord_y;
        }

        while (Game_board[coord_y - 1][coord_x - 1].taken)
        {
            std::cout << "Please Enter different coordinates, those are taken\n";
            std::cin >> coord_x >> coord_y;
            if (coord_x > Game_board.GetSize() || coord_x < 0) continue;
            if (coord_y > Game_board.GetSize() || coord_y < 0) continue;
        }

        Game_board[(coord_y-1)][(coord_x-1)].Set(Game_player_choice);
        std::cout << Game_board << std::endl;
        if (-10 != ScanBoard()){break;}

        std::cout << "AI's Turn\n";
        Game_AI.Turn(First);
        std::cout << Game_board << std::endl;

        if (-10 != ScanBoard()){break;}
    }
}