#ifndef AI_PAMSI_GAME_H
#define AI_PAMSI_GAME_H
#include "Board.h"
#include "AI.h"

class Game  //Klasa Gra
{
private:
    Board& Game_board;                                    //Plansze na ktorej toczy sie rozgrywka
    AI Game_AI;                                           //Algorytmy sztucznej inteligencji dla komputera
    char Game_player_choice;                              //Znacznik gracza
    char AI_Icon;                                         //Znacznik Komputera
    int ScanBoard();                                      //Metoda bada do ostatniego wolnego pola czy ktos wygral
public:
    ~Game()= default;                                     //Domyslny destruktor klasy Gra
    Game(Board& board, AI ai, char player_choice);        //Konstruktor parametryczny Gry - ustawia warunki poczatkowe
    Board& GetBoard(){return this->Game_board;};          //Metoda dostepu do zbadania rozgrywki
    void Play();                                          //Metoda sterujaca dzialaniem gry oraz zabezpieczenie
};
#endif //AI_PAMSI_GAME_H
