#include "Board.h"

Board::Board() : Board_Size(3)
{
    board = std::vector<std::vector<Cell>>(3, std::vector<Cell>(3, ' '));
}

Board::Board(int size): Board_Size(size)
{
    board = std::vector<std::vector<Cell>>(size, std::vector<Cell>(size, ' '));
}

void Board::Clean()
{
    for (int i = 0; i < Board_Size; i++)
    {
        for (int j = 0; j < Board_Size; j++)
            this->board[i][j].Free();
    }
}

std::vector<Cell> Board::operator[](const int index) const
{
    if (index > Board_Size - 1 || index < 0)
    {
        std::cerr << "index ouf of range! index: " << index << std::endl;
        return board[index % this->Board_Size];
    }
    else
        return board[index];
}

std::vector<Cell>& Board::operator[](const int index)
{
    if (index > Board_Size - 1 || index < 0)
    {
        std::cerr << "index ouf of range! index: " << index << std::endl;
        return board[index % this->Board_Size];
    }
    else
        return board[index];
}

std::ostream& operator<<(std::ostream& stream, const Board& board_stream)
{
    stream << "\n  ";
    for (int j = 0; j < board_stream.GetSize()+1; j++)
        stream << j << " | ";
    stream << std::endl;

    stream << "  ";
    for (int k = 0; k < board_stream.GetSize() * 4.5; k++)
        stream << "-";
    stream << std::endl;

    for (int i = 0; i < board_stream.GetSize(); i++)
    {
        stream << "  " << i+1;
        for (int j = 0; j < board_stream.GetSize(); j++)
            stream << " | " << board_stream[i][j];
        stream << " |"<< std::endl;

        stream << "  ";
        for (int k = 0; k < board_stream.GetSize() * 4.5; k++)
            stream << "-";
        stream << std::endl;
    }
    return stream;
}

