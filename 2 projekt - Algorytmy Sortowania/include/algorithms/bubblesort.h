#ifndef SORTING_ALGORITHMS_BUBBLESORT_H
#define SORTING_ALGORITHMS_BUBBLESORT_H
#include <vector>
#include <algorithm>

template <typename T>
class BubbleSort    //sortowanie bąbelkowe
{
public:
    void sort(typename std::vector<T>::iterator start, typename std::vector<T>::iterator end);
};

template <typename T>
void BubbleSort<T>::sort(typename std::vector<T>::iterator start, typename std::vector<T>::iterator end) // min-->max
{
    for(auto iterator_i = start; iterator_i != end -1 ; iterator_i++) // end-1 aby nie wyjsc za tablice
        for(auto iterator_j = start; iterator_j !=end-1; iterator_j++)
        {
            if (*iterator_j > *(iterator_j + 1)) //czy poprzedni wiekszy
            {
                std::swap(*iterator_j, *(iterator_j + 1)); //zamiana adresow sasiadow
            }
        }
}
#endif //SORTING_ALGORITHMS_BUBBLESORT_H
