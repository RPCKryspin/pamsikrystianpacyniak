#ifndef SORTING_ALGORITHMS_INSERTSORT_H
#define SORTING_ALGORITHMS_INSERTSORT_H
#include <vector>

template <typename T>
class InsertSort    //sortowanie przez wstawianie
{
public:
    void sort(typename std::vector<T>::iterator start, typename std::vector<T>::iterator end);
};

template <typename T>
void InsertSort<T>::sort(typename std::vector<T>::iterator start, typename std::vector<T>::iterator end) // min-->max
{
    T temporary;
    for (auto iterator_i = start; iterator_i != end; iterator_i++)
    {
        auto iterator_j = iterator_i;
        temporary = *iterator_i; // pobieramy adres
        while(iterator_j != start && temporary < *(iterator_j - 1))//lewy warunek zabezpiecza przed wyjsciem z tablicy
        {
            *iterator_j = *(iterator_j - 1); // wstawianie (po sprawdzeniu czy poprzedni element wiekszy)
            iterator_j--;
        }
        *iterator_j = temporary; //oddajemy adres w miejsce starej wartosci
    }
}
#endif //SORTING_ALGORITHMS_INSERTSORT_H
