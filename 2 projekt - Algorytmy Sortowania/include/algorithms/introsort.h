#ifndef SORTING_ALGORITHMS_INTROSORT_H
#define SORTING_ALGORITHMS_INTROSORT_H
#include <vector>
#include <cmath>
#include <algorithm>
#include "insertsort.h"
#include "heapsort.h"

template <typename T>
class IntroSort // sortowanie introspektywne
{
    HeapSort<T> Hsort;
    InsertSort<T> Isort;
    int kontrolka=0;
public:
    typename std::vector<T>::iterator partition(typename std::vector<T>::iterator start, typename std::vector<T>::iterator end);
    void Hybrid_Introspective_Sort(typename std::vector<T>::iterator start, typename std::vector<T>::iterator end, int M);
    void sort(typename std::vector<T>::iterator start, typename std::vector<T>::iterator end);
};

template <typename T>
void IntroSort<T>::sort(typename std::vector<T>::iterator start, typename std::vector<T>::iterator end) // min-->max
{
    Hybrid_Introspective_Sort(start,end,(int)floor(2*log(std::distance(start, end))/log(2)));
    Isort.sort(start,end);
}
template <typename T>
void IntroSort<T>::Hybrid_Introspective_Sort(typename std::vector<T>::iterator start, typename std::vector<T>::iterator end, int M)
{
    if (M<=0)
    {
        Hsort.sort(start, end);
        return;
    }
    if(!kontrolka){end--;kontrolka++;}  // Przeskalowanie aby ostatni element nie wylatywał za tablice
    auto part = partition(start, end);  // wazna dla sekcji z partition bo heapsorta jest zabezpieczone wewnatrz

    if (std::distance(start, part)>9)
        Hybrid_Introspective_Sort(start,part-1,M-1);

    if ((std::distance(part,end)-1)>9)
        Hybrid_Introspective_Sort(part+1,end,M-1);
}
template <typename T>
typename std::vector<T>::iterator IntroSort<T>::partition(typename std::vector<T>::iterator start, typename std::vector<T>::iterator end)
{
    T pivot = *end; // select the rightmost element as pivot
    auto iterator_i = start-1; // pointer for greater element
    auto iterator_j = start;

    while( iterator_j < end) // traverse each element of the array & compare them with the pivot
    {
        if (*iterator_j <= pivot)  // if element smaller than pivot is, swap it with the greater element pointed by i
        {
            iterator_i++;
            std::swap(*iterator_i, *iterator_j);
        }
        iterator_j++;
    }

    std::swap(*(iterator_i+1), *end); //zamiana adresow sasiadow - swap pivot with the greater element at i
    return (iterator_i + 1);// return the partition point
}
#endif //SORTING_ALGORITHMS_INTROSORT_H
