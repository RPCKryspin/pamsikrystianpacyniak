#ifndef SORTING_ALGORITHMS_QUICKSORT_H
#define SORTING_ALGORITHMS_QUICKSORT_H
#include <vector>
#include <algorithm>

template <typename T>
class QuickSort // quicksort
{
    int kontrolka=0;
public:
    typename std::vector<T>::iterator partition(typename std::vector<T>::iterator start, typename std::vector<T>::iterator end);
    void sort(typename std::vector<T>::iterator start, typename std::vector<T>::iterator end);
};

template <typename T> // function to rearrange array (find the partition point)
typename std::vector<T>::iterator QuickSort<T>::partition(typename std::vector<T>::iterator start, typename std::vector<T>::iterator end)
{
    T pivot = *end; // select the rightmost element as pivot
    auto iterator_i = start-1; // pointer for greater element
    auto iterator_j = start;

    while( iterator_j < end) // traverse each element of the array & compare them with the pivot
    {
        if (*iterator_j <= pivot)  // if element smaller than pivot is, swap it with the greater element pointed by i
        {
            iterator_i++;
            std::swap(*iterator_i, *iterator_j);
        }
        iterator_j++;
    }

    std::swap(*(iterator_i+1), *end); //zamiana adresow sasiadow - swap pivot with the greater element at i
    return (iterator_i + 1);// return the partition point
}

template <typename T>
void QuickSort<T>::sort(typename std::vector<T>::iterator start, typename std::vector<T>::iterator end) // min-->max
{
    if(!kontrolka){end--;kontrolka++;}  // Przeskalowanie aby ostatni element nie wylatywał za tablice
    if (start < end)
    {
        auto part = partition(start, end); // find the pivot element such that

        // elements smaller than pivot are on left of pivot and greater than pivot are on righ
        sort(start,part-1);  // recursive call on the left of pivot
        sort(part + 1, end);    // recursive call on the right of pivot
    }
}


#endif //SORTING_ALGORITHMS_QUICKSORT_H
