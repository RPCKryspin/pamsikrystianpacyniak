#ifndef SORTING_ALGORITHMS_SHELLSORT_H
#define SORTING_ALGORITHMS_SHELLSORT_H
#include <vector>

template <typename T>
class ShellSort // sortowanie Shella
{
public:
    void sort(typename std::vector<T>::iterator start, typename std::vector<T>::iterator end);
};

template <typename T>
void ShellSort<T>::sort(typename std::vector<T>::iterator start, typename std::vector<T>::iterator end) // min-->max
{
    std::size_t number_elements = std::distance(start, end);
    // Rearrange elements at each n/2, n/4, n/8, ... intervals,gaps --> last parameter in first for
    for (std::size_t interval = number_elements / 2; interval > 0; interval /= 2)
    {
        for (auto iterator_i = start + interval; iterator_i < end; iterator_i++)
        {
            T temp = *iterator_i;
            auto iterator_j=iterator_i;
            while ( iterator_j >= start+interval && *(iterator_j - interval) > temp )
            {
                *iterator_j = *(iterator_j - interval);
                iterator_j -= interval;
            }
            *iterator_j = temp;
        }
    }
}

#endif //SORTING_ALGORITHMS_SHELLSORT_H
