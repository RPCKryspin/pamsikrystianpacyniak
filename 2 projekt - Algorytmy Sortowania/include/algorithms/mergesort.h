#ifndef SORTING_ALGORITHMS_MERGESORT_H
#define SORTING_ALGORITHMS_MERGESORT_H
#include <vector>

template <typename T>
class MergeSort // sortowanie przez scalanie
{
    int kontrolka=0;
public:
    void merge(typename std::vector<T>::iterator start, typename std::vector<T>::iterator middle, typename std::vector<T>::iterator end); // metoda procedury scalania
    void sort(typename std::vector<T>::iterator start, typename std::vector<T>::iterator end);  // Metoda sterujaca
};

template <typename T>
void MergeSort<T>::merge(typename std::vector<T>::iterator start, typename std::vector<T>::iterator middle, typename std::vector<T>::iterator end)
{
    // Create L ← A[p..q] and M ← A[q+1..r]
    std::size_t n1 = std::distance(start, middle)+1;
    std::size_t n2 = std::distance(middle, end);
    auto iterator_i = start;
    auto iterator_j = middle;

    T L[n1], M[n2]; // create subarrays - strategy devide and win
    for (int i = 0; i < n1; i++)
        L[i] = *(iterator_i+i);
    for (int j = 0; j < n2; j++)
        M[j] = *(iterator_j+j+1);

    // Until we reach either end of either L or M, pick larger among
    // elements L and M and place them in the correct position at A[p..r]
    int i=0, j=0;
    auto iterator_k=start;
    while (i < n1 && j < n2)    // scalanie czesciowe
    {
        if (L[i] <= M[j])
        {
            *iterator_k = L[i];
            i++;
        }
        else if (L[i] > M[j])
        {
            *iterator_k = M[j];
            j++;
        }
        if(iterator_k==end){break;}
        iterator_k++;
    }

    // When we run out of elements in either L or M,
    // pick up the remaining elements and put in A[p..r]
    while (i < n1||j < n2)
    {
        if (i < n1)
        {
            *iterator_k = L[i];
            i++;
        }
        else if (j < n2)
        {
            *iterator_k = M[j];
            j++;
        }
       if(iterator_k==end){break;}
        iterator_k++;
    }
}

template <typename T>
void MergeSort<T>::sort(typename std::vector<T>::iterator start, typename std::vector<T>::iterator end) // min-->max
{
    if(!kontrolka){end--;kontrolka++;}  // Przeskalowanie aby ostatni element nie wylatywał za tablice
    if (start < end)
    {                   // middle is the point where the array is divided into two subarrays
        typename std::vector<T>::iterator middle = std::next(start, std::distance(start, end) / 2);
        sort( start, middle);   // Rekurencyjne podziały
        sort( middle+1, end);
        merge(start, middle, end); // Merge the sorted subarrays
    }
}
#endif //SORTING_ALGORITHMS_MERGESORT_H
