#include <iostream>
#include <vector>
#include <chrono>
#include <cmath>
#include <cstdlib>
#include <unistd.h>
#include <ctime>

#include "algorithms/bubblesort.h"
#include "algorithms/insertsort.h"
#include "algorithms/heapsort.h"
#include "algorithms/mergesort.h"
#include "algorithms/shellsort.h"
#include "algorithms/quicksort.h"
#include "algorithms/introsort.h"

//using namespace std;

BubbleSort<int> BubbleS;
InsertSort<int> InsertS;
HeapSort<int> HeapS;
MergeSort<int> MergeS;
ShellSort<int> ShellS;
QuickSort<int> QuickS;
IntroSort<int> IntroS;

std::vector<int> Testy_Tablica;
std::vector<int> Glowna_Tablica;
std::vector<int> Kopia_Glowna_Tablica;
std::vector<int> Kopia_Glowna_Tablica_przygotowana;
std::vector<int> Ilosc_elementow{10000, 50000, 100000, 500000, 1000000};
const int Liczba_Tablic = 10;
double warunek=0; // warunki poczatkowe
bool inited_data = false;
bool polarization_minimum_maximum = true;

void global_init_data_maximum()
{
    int x;
    srand((unsigned)time(nullptr));
    for( int i = 0; i < Ilosc_elementow.back(); i++)
    {
        x = rand() % 99 +1;//1-99
        Glowna_Tablica.push_back(x);
    }
}
void Skaluj(int nr_elem)
{
    Kopia_Glowna_Tablica.clear();
    for(int i=0;i<nr_elem;i++)
    {
        Kopia_Glowna_Tablica.push_back(Glowna_Tablica[i]);
    }

}
void Przygotuj(int nr_elem)
{
    Skaluj(nr_elem);
    if(warunek!=0 && warunek!=1)
    {
        QuickS.sort(Kopia_Glowna_Tablica.begin(), Kopia_Glowna_Tablica.begin() + (int) (nr_elem * warunek));
    }
    else if (warunek==1 && polarization_minimum_maximum==false)
    {
        QuickS.sort(Kopia_Glowna_Tablica.begin(), Kopia_Glowna_Tablica.end());
        std::reverse(Kopia_Glowna_Tablica.begin(),Kopia_Glowna_Tablica.end());
    }
    /* Reverse dla tablic polega na swapowaniu elementu start+i z elementem end-i dla i=0 do i=nr elements/2   *
     * od tego mamy parametry start i end ktory mozna manipulowac ale tym razem skorzystam z wbudowanej metody */
}

void Testowanie_Algorytmow()
{
    const int Nr_elements = 50000; // Tu mozna zmienic liczebnosc elementow do posortowania //
    while (true)
    {
        std::cout << std::endl << std::endl;
        std::cout << "Menu Wizualnych testow Algorytmow Sortowania " << std::endl;
        std::cout << "Program bedzie po kazdym tescie zmienial dane wejsciowe" << std::endl;
        std::cout << "Testy zawieraja domyslnie 50000 elementow" << std::endl;
        std::cout << "Wybierz jakie chcesz przetestowac sortowanie" << std::endl << std::endl << std::endl;
        std::cout << "Wpisz cyfre jeden jesli Sortowanie Babelkowe" << std::endl;
        std::cout << "Wpisz cyfre dwa jesli Sortowanie przez Wstawianie" << std::endl;
        std::cout << "Wpisz cyfre trzy jesli Sortowanie przez kopcowanie" << std::endl;
        std::cout << "Wpisz cyfre cztery jesli Sortowanie przez scalanie" << std::endl;
        std::cout << "Wpisz cyfre piec jesli Sortowanie Shella" << std::endl;
        std::cout << "Wpisz cyfre szesc jesli Sortowanie Szybkie" << std::endl;
        std::cout << "Wpisz cyfre siedem jesli Sortowanie Introspektywne" << std::endl;
        std::cout << "Wpisz cyfre osiem aby zakonczyc testowanie i wrocic do badan" << std::endl << std::endl;

        int wyborTest;
        std::cout << "Wybieram numer:  ";
        std::cin >> wyborTest;
        while (std::cin.fail() || wyborTest > 8 || wyborTest <= 0)
        {
            std::cout << "Nie znam takiego polecenia - wprowadz ponownie" << std::endl;
            std::cin.clear();
            std::cin.ignore(100, '\n');
            std::cout << std::endl << "Wybieram numer:  ";
            std::cin >> wyborTest;
        }
        if(wyborTest==8)
        {
            std::cout << std::endl << "Wracam do menu Badan Efektywnosci" << std::endl<< std::endl;
            break;
        }

        std::cout << "Teraz wpisz 1 jesli chcesz sortowac w kolejnosc min->max lub 0 aby odwrotnie" << std::endl;
        int wyborTestpolarization;
        std::cout << "Wybieram numer:  ";
        std::cin >> wyborTestpolarization;
        while (std::cin.fail() || wyborTestpolarization > 1 || wyborTestpolarization < 0)
        {
            std::cout << "Nie znam takiego polecenia - wprowadz ponownie" << std::endl;
            std::cin.clear();
            std::cin.ignore(100, '\n');
            std::cout << std::endl << "Wybieram numer:  ";
            std::cin >> wyborTestpolarization;
        }
        if(wyborTestpolarization==1){polarization_minimum_maximum=true;}
        else {polarization_minimum_maximum=false;}

        srand((unsigned)time(nullptr));
        for( int i = 0; i < Nr_elements; i++)
        {
            int x = rand() % 99 +1;//1-99
            Testy_Tablica.push_back(x);
        }
        std::cout << std::endl ;
        std::cout << "Oto nasze dane wejsciowe" << std::endl<<std::endl;
        for(int i = 0; i < Nr_elements; i++)
        {
            std::cout  << Testy_Tablica[i]<<" ";
            if(i%25==24){std::cout<<std::endl;}
        }

        switch(wyborTest)
        {
            case 1:
                BubbleS.sort(Testy_Tablica.begin(), Testy_Tablica.end());
                if(!polarization_minimum_maximum){std::reverse(Testy_Tablica.begin(),Testy_Tablica.end());}
                break;
            case 2:
                InsertS.sort(Testy_Tablica.begin(), Testy_Tablica.end());
                if(!polarization_minimum_maximum){std::reverse(Testy_Tablica.begin(),Testy_Tablica.end());}
                break;
            case 3:
                HeapS.sort(Testy_Tablica.begin(), Testy_Tablica.end());
                if(!polarization_minimum_maximum){std::reverse(Testy_Tablica.begin(),Testy_Tablica.end());}
                break;
            case 4:
                MergeS.sort(Testy_Tablica.begin(), Testy_Tablica.end());
                if(!polarization_minimum_maximum){std::reverse(Testy_Tablica.begin(),Testy_Tablica.end());}
                break;
            case 5:
                ShellS.sort(Testy_Tablica.begin(), Testy_Tablica.end());
                if(!polarization_minimum_maximum){std::reverse(Testy_Tablica.begin(),Testy_Tablica.end());}
                break;
            case 6:
                QuickS.sort(Testy_Tablica.begin(), Testy_Tablica.end());
                if(!polarization_minimum_maximum){std::reverse(Testy_Tablica.begin(),Testy_Tablica.end());}
                break;
            case 7:
                IntroS.sort(Testy_Tablica.begin(), Testy_Tablica.end());
                if(!polarization_minimum_maximum){std::reverse(Testy_Tablica.begin(),Testy_Tablica.end());}
                break;
            default:
                break;
        }

        std::cout <<std::endl<< "Oto nasze posortowane dane wejsciowe" << std::endl<< std::endl;
        for (int i = 0; i < Nr_elements; i++)
        {
            std::cout << Testy_Tablica[i] << " ";
            if (i % 25 == 24) { std::cout << std::endl; }
        }
        Testy_Tablica.clear();
    }
}

void Badaj_BubbleSort(int n)
{
    Przygotuj(n);
    for (int i = 0; i < Liczba_Tablic; ++i)
    {
        Kopia_Glowna_Tablica_przygotowana=Kopia_Glowna_Tablica;
        auto start = std::chrono::system_clock::now();
        BubbleS.sort(Kopia_Glowna_Tablica_przygotowana.begin(), Kopia_Glowna_Tablica_przygotowana.end());
        auto end = std::chrono::system_clock::now();
        std::chrono::duration<double> diff = (end - start) * 1000; // w sekundach a dla ms diff * 1000
        double durationTime = diff.count();
        std::cout << durationTime << std::endl;
    }
}

void Badaj_InsertSort(int n)
{
    Przygotuj(n);
    for (int i = 0; i < Liczba_Tablic; ++i)
    {
        Kopia_Glowna_Tablica_przygotowana=Kopia_Glowna_Tablica;
        auto start = std::chrono::system_clock::now();
        InsertS.sort(Kopia_Glowna_Tablica_przygotowana.begin(), Kopia_Glowna_Tablica_przygotowana.end());
        auto end = std::chrono::system_clock::now();
        std::chrono::duration<double> diff = (end - start) * 1000; // w sekundach a dla ms diff * 1000
        double durationTime = diff.count();
        std::cout << durationTime << std::endl;
    }
}

void Badaj_HeapSort(int n)
{
    Przygotuj(n);
    for (int i = 0; i < Liczba_Tablic; ++i)
    {
        Kopia_Glowna_Tablica_przygotowana=Kopia_Glowna_Tablica;
        auto start = std::chrono::system_clock::now();
        HeapS.sort(Kopia_Glowna_Tablica_przygotowana.begin(), Kopia_Glowna_Tablica_przygotowana.end());
        auto end = std::chrono::system_clock::now();
        std::chrono::duration<double> diff = (end - start) * 1000; // w sekundach a dla ms diff * 1000
        double durationTime = diff.count();
        std::cout << durationTime << std::endl;
    }
}

void Badaj_MergeSort(int n)
{
    Przygotuj(n);
    for (int i = 0; i < Liczba_Tablic; ++i)
    {
        Kopia_Glowna_Tablica_przygotowana=Kopia_Glowna_Tablica;
        auto start = std::chrono::system_clock::now();
        MergeS.sort(Kopia_Glowna_Tablica_przygotowana.begin(), Kopia_Glowna_Tablica_przygotowana.end());
        auto end = std::chrono::system_clock::now();
        std::chrono::duration<double> diff = (end - start) * 1000; // w sekundach a dla ms diff * 1000
        double durationTime = diff.count();
        std::cout << durationTime << std::endl;
    }
}

void Badaj_ShellSort(int n)
{
    Przygotuj(n);
    for (int i = 0; i < Liczba_Tablic; ++i)
    {
        Kopia_Glowna_Tablica_przygotowana=Kopia_Glowna_Tablica;
        auto start = std::chrono::system_clock::now();
        ShellS.sort(Kopia_Glowna_Tablica_przygotowana.begin(), Kopia_Glowna_Tablica_przygotowana.end());
        auto end = std::chrono::system_clock::now();
        std::chrono::duration<double> diff = (end - start) * 1000; // w sekundach a dla ms diff * 1000
        double durationTime = diff.count();
        std::cout << durationTime << std::endl;
    }
}

void Badaj_QuickSort(int n)
{
    Przygotuj(n);
    for (int i = 0; i < Liczba_Tablic; ++i)
    {
        Kopia_Glowna_Tablica_przygotowana=Kopia_Glowna_Tablica;
        auto start = std::chrono::system_clock::now();
        QuickS.sort(Kopia_Glowna_Tablica_przygotowana.begin(), Kopia_Glowna_Tablica_przygotowana.end());
        auto end = std::chrono::system_clock::now();
        std::chrono::duration<double> diff = (end - start) * 1000; // w sekundach a dla ms diff * 1000
        double durationTime = diff.count();
        std::cout << durationTime << std::endl;
    }
}

void Badaj_IntroSort(int n)
{
    Przygotuj(n);
    for (int i = 0; i < Liczba_Tablic; ++i)
    {
        Kopia_Glowna_Tablica_przygotowana=Kopia_Glowna_Tablica;
        auto start = std::chrono::system_clock::now();
        IntroS.sort(Kopia_Glowna_Tablica_przygotowana.begin(), Kopia_Glowna_Tablica_przygotowana.end());
        auto end = std::chrono::system_clock::now();
        std::chrono::duration<double> diff = (end - start) * 1000; // w sekundach a dla ms diff * 1000
        double durationTime = diff.count();
        std::cout << durationTime << std::endl;
    }
}

int main(int argc, char* argv[])
{
    while(true)
    {
        polarization_minimum_maximum=true;

        std::cout << "Algorytmy Sortowania - Menu Badan Zlozonosci Czasowej " << std::endl << std::endl;
        std::cout << "Tablica bedzie zawierala te same elementy w zaleznosci od rozmiaru" << std::endl;
        std::cout << "Badania domyslnie prowadzone sa dla sortowania w kolejnosci min->max"<< std::endl;
        std::cout << "Wybierz jaki chcesz zbadac Algorytm" << std::endl << std::endl;
        std::cout << "Wpisz cyfre jeden jesli Sortowanie Babelkowe" << std::endl;
        std::cout << "Wpisz cyfre dwa jesli Sortowanie przez Wstawianie" << std::endl;
        std::cout << "Wpisz cyfre trzy jesli Sortowanie przez kopcowanie" << std::endl;
        std::cout << "Wpisz cyfre cztery jesli Sortowanie przez scalanie" << std::endl;
        std::cout << "Wpisz cyfre piec jesli Sortowanie Shella" << std::endl;
        std::cout << "Wpisz cyfre szesc jesli Sortowanie Szybkie" << std::endl;
        std::cout << "Wpisz cyfre siedem jesli Sortowanie Introspektywne" << std::endl;
        std::cout << "Wpisz cyfre osiem aby przejsc do menu testow wizualnych Algorytmow Sorotwania" << std::endl;
        std::cout << "Wpisz cyfre dziewiec aby zakonczyc Badania" << std::endl;

        int wyborBadania;
        std::cout << std::endl << "Wybieram numer:  ";
        std::cin >> wyborBadania;
        while (std::cin.fail() || wyborBadania > 9 || wyborBadania <= 0)
        {
            std::cout << "Nie znam takiego polecenia - wprowadz ponownie" << std::endl;
            std::cin.clear();
            std::cin.ignore(100, '\n');
            std::cout << std::endl << "Wybieram numer:  ";
            std::cin >> wyborBadania;
        }
        if(wyborBadania==9)
        {std::cout << std::endl << "Koniec Dzialania Programu" << std::endl<< std::endl; break;}

        if(wyborBadania==8){Testowanie_Algorytmow();continue;}

        std::cout << "Teraz wybierz w jakich warunkach tablice chcesz zbadac. Wpisz liczbe:" << std::endl;
        std::cout << "jeden - 25% elementow poczatkowych juz posortowanych" << std::endl;
        std::cout << "dwa - 50% elementow poczatkowych juz posortowanych" << std::endl;
        std::cout << "trzy - 75% elementow poczatkowych juz posortowanych" << std::endl;
        std::cout << "cztery - 95% elementow poczatkowych juz posortowanych" << std::endl;
        std::cout << "piec - 99% elementow poczatkowych juz posortowanych" << std::endl;
        std::cout << "szesc - 99,7% elementow poczatkowych juz posortowanych" << std::endl;
        std::cout << "siedem - 100% elementow juz posortowanych ,ale w odworntej kolejnosci" << std::endl;
        std::cout << "osiem - 0% elementow poczatkowych juz posortowanych ( elementy losowe )" << std::endl;
        std::cout << "dziewiec aby zrezygnowac z badania" << std::endl;

        double WybWar;
        std::cout << std::endl << "Wybieram numer:  ";
        std::cin >> WybWar;
        while (std::cin.fail() || WybWar >9 || WybWar <= 0)
        {
            std::cout << "Nie znam takiego polecenia - wprowadz ponownie" << std::endl;
            std::cin.clear();
            std::cin.ignore(100, '\n');
            std::cout << std::endl << "Wybieram numer:  ";
            std::cin >> WybWar;
        }
        std::cout << std::endl << std::endl;

        if(WybWar==9)
        {std::cout << std::endl << "Koniec Dzialania Programu" << std::endl<< std::endl; break;}

        if(WybWar==1){warunek=0.25;}if(WybWar==2){warunek=0.50;}if(WybWar==3){warunek=0.75;}
        if(WybWar==4){warunek=0.95;}if(WybWar==5){warunek=0.99;}if(WybWar==6){warunek=0.997;}
        if(WybWar==7){warunek=1;polarization_minimum_maximum=false;}if(WybWar==8){warunek=0;}

        if(!inited_data){global_init_data_maximum();inited_data=true;}

        switch (wyborBadania)
        {
            case 1:
                for (const auto &n : Ilosc_elementow) // Badanie dla różnej liczby operacji
                {
                    std::cout << "Badam tablice " << n << " elementowe w jednostce [ms]" << std::endl;
                    Badaj_BubbleSort(n);
                }
                break;
            case 2:
                for (const auto &n : Ilosc_elementow) // Badanie dla różnej liczby operacji
                {
                    std::cout << "Badam tablice " << n << " elementowe w jednostce [ms]" << std::endl;
                    Badaj_InsertSort(n);
                }
                break;
            case 3:
                for (const auto &n : Ilosc_elementow) // Badanie dla różnej liczby operacji
                {
                    std::cout << "Badam tablice " << n << " elementowe w jednostce [ms]" << std::endl;
                    Badaj_HeapSort(n);
                }
                break;
            case 4:
                for (int j=0;j<Ilosc_elementow.size()-1;j++) // Badanie dla różnej liczby operacji
                {
                    std::cout << "Badam tablice " << Ilosc_elementow[j] << " elementowe w jednostce [ms]" << std::endl;
                    Badaj_MergeSort(Ilosc_elementow[j]);
                }
                break;
            case 5:
                for (const auto &n : Ilosc_elementow) // Badanie dla różnej liczby operacji
                {
                    std::cout << "Badam tablice " << n << " elementowe w jednostce [ms]" << std::endl;
                    Badaj_ShellSort(n);
                }
                break;
            case 6:
                for (const auto &n : Ilosc_elementow) // Badanie dla różnej liczby operacji
                {
                    std::cout << "Badam tablice " << n << " elementowe w jednostce [ms]" << std::endl;
                    Badaj_QuickSort(n);
                }
                break;
            case 7:
                for (const auto &n : Ilosc_elementow) // Badanie dla różnej liczby operacji
                {
                    std::cout << "Badam tablice " << n << " elementowe w jednostce [ms]" << std::endl;
                    Badaj_IntroSort(n);
                }
                break;
            default:
                break;
        }
    }

    return 0;
}

