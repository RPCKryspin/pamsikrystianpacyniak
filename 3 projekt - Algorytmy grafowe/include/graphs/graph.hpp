#ifndef GRAPH_HPP_
#define GRAPH_HPP_
//ifndef define endif = pragma once

#include "graphs/vertex.hpp"
#include "graphs/edge.hpp"

class Graph   //Klasa Graf
{
  protected:  //Hermetyzacja: dostępne tylko dla klas dziedziczących i dla programisty
    std::vector<std::shared_ptr<vertex>> VertexList;            // numeracja wierchołków i kotwiczenie
    std::vector<std::shared_ptr<edge>> EdgeList;                // numeracja krawędzi i kotwiczenie
    int n, m;                                                   // Liczba wierzcholkow i krawedzi
  public:
    /*
     * Polimorfizm: deklaracje istnienia metod dzieki czemu można wywolać w Graph metode,
     * jesli zostala zainicjalizowana jakas klasa potomna dziedziczaca bedaca reprezentacja grafu
     */
    virtual std::shared_ptr<edge> insertEdge(std::shared_ptr<vertex>a,        //Metoda Uaktualniajaca: Dodaj krawedz
                                             std::shared_ptr<vertex>b, int w) = 0;
    virtual void eraseEdge(std::shared_ptr<edge> e) = 0;                      //Metoda Uaktualniajaca: Usun krawedz
    virtual std::shared_ptr<vertex> insertVertex(int w) = 0;                  //Metoda Uaktualniajaca: Dodaj wierzcholek
    virtual void eraseVertex(std::shared_ptr<vertex> v) = 0;                  //Metoda Uaktualniajaca: Usun wierzcholek
    std::shared_ptr<vertex> Vertex_at(int i){return VertexList[i];};          //Metoda zwraca wskaznik na szukany wierzcholek
    virtual std::shared_ptr<edge> getEdge(std::shared_ptr<vertex> p,          //Metoda zwraca wskaznik na szukana krawedz (wierzcholki)
                                          std::shared_ptr<vertex> k) = 0;
    virtual void print_graph() = 0;                                           //Metoda wypisuje graf
};
#endif /* GRAPH_HPP_ */
