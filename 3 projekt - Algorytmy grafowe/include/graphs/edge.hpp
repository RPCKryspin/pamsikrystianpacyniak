#ifndef GRAPH_ALGORITHMS_EDGE_HPP
#define GRAPH_ALGORITHMS_EDGE_HPP
#include <utility>
class edge     //Klasa Krawedz Grafu
{
  public:
    int weight;                                                                 // Waga krawedzi
    int ID;                                                                     // Numer porzadkowy krawedzi
    std::shared_ptr<vertex> v,w;                                                // Wierzcholek startowy i koncowy

    edge( std::shared_ptr<vertex> v ,std::shared_ptr<vertex> w,int v_,int ID)   //Konstruktor klasy edge
        {this->v=std::move(v); this->w=std::move(w); this->weight=v_;this->ID=ID;}
    ~edge() = default;                                                          //Destruktor klasy edge
};
#endif // GRAPH_ALGORITHMS_EDGE_HPP
