#ifndef ADJACENCY_MATRIX_GRAPH_HPP_
#define ADJACENCY_MATRIX_GRAPH_HPP_

#include <memory>
#include <utility>
#include <vector>
#include "graphs/graph.hpp"

class AdjacencyMatrixGraph : public Graph   //Klasa Macierz Sasiedztwa
{
  private:
    std::vector<std::vector<std::shared_ptr<edge> > > AdjacencyMatrix;    //Macierz sasiedztwa
  public:
    AdjacencyMatrixGraph(int n, int m);                                   //Konstruktor Klasy Macierz Sasiedztwa
    std::shared_ptr<vertex> insertVertex(int w) override;                 //Nadpisanie metody wirtualnej dodaj wierzcholek
    void eraseVertex(std::shared_ptr<vertex> v) override;                 //Nadpisanie metody wirtualnej usun wierzcholek
    std::shared_ptr<edge> insertEdge(std::shared_ptr<vertex>a,            //Nadpisanie metody wirtualnej  dodaj krawedz
                                     std::shared_ptr<vertex>b, int w) override;
    void eraseEdge(std::shared_ptr<edge> e) override;                     //Nadpisanie metody wirtualnej  usun wierzcholek
    std::shared_ptr<edge> getEdge( std::shared_ptr<vertex> p,             //Nadpisanie metody wirtualnej podaj krawedz
                                  std::shared_ptr<vertex> k) override;
    static std::unique_ptr<Graph> createGraph(std::istream& is);          //Metoda tworzaca graf z danych ze strumienia
    void print_graph() override;                                          //Metoda wypisuje graf na konsoli
};
#endif /* ADJACENCY_MATRIX_GRAPH_HPP_ */
