#ifndef ADJACENCY_LIST_GRAPH_HPP_
#define ADJACENCY_LIST_GRAPH_HPP_

#include <memory>
#include <utility>
#include <vector>
#include "graphs/graph.hpp"

class AdjacencyListGraph : public Graph    //Klasa Lista Sasiedztwa
{
  private:
    std::vector<std::vector<std::shared_ptr<edge>> > AdjacencyList;   //Lista Sasiedztwa
  public:
    AdjacencyListGraph(int n, int m);                                 //Konstruktor Klasy Lista Sasiedztwa
    std::shared_ptr<vertex> insertVertex(int w) override;             //Nadpisanie metody wirtualnej dodaj wierzcholek
    void eraseVertex(std::shared_ptr<vertex> v) override;             //Nadpisanie metody wirtualnej usun wierzcholek
    std::shared_ptr<edge> insertEdge(std::shared_ptr<vertex>a,        //Nadpisanie metody wirtualnej  dodaj krawedz
                                     std::shared_ptr<vertex>b, int w) override;
    void eraseEdge(std::shared_ptr<edge> e) override;                 //Nadpisanie metody wirtualnej  usun wierzcholek
    std::shared_ptr<edge> getEdge( std::shared_ptr<vertex> p,
                                  std::shared_ptr<vertex> k) override;//Nadpisanie metody wirtualnej podaj krawedz
    static std::unique_ptr<Graph> createGraph(std::istream& is);      //Metoda tworzaca graf z danych ze strumienia
    void print_graph() override;                                      //Metoda wypisuje graf na konsoli
};
#endif /* ADJACENCY_LIST_GRAPH_HPP_ */