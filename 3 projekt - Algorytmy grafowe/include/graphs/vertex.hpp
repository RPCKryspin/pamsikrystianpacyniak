#ifndef GRAPH_ALGORITHMS_VERTEX_HPP
#define GRAPH_ALGORITHMS_VERTEX_HPP
class vertex    //Klasa Wierzcholek Grafu
{
  public:
    int value;                                       // wartosc elementu w wierzcholek
    int ID;                                          // numer porzadkowy wierzolka
    vertex(int v,int ID){this->value=v;this->ID=ID;} // konstruktor klasy vertex
    ~vertex() = default;                             // Automatyczny konstruktor klasy vertex
};
#endif // GRAPH_ALGORITHMS_VERTEX_HPP
