#ifndef GRAPH_ALGORITHMS_FILEREAD_HPP
#define GRAPH_ALGORITHMS_FILEREAD_HPP

#include <fstream>
#include <istream>
#include <string>

class FileRead // Klasa wspomagajaca wczytywanie grafu
{
    std::ifstream file;                                                    //Plik z danymi wejsciowymi
  public:
    explicit FileRead(const std::string& filename)
                        {file.open(filename,std::ifstream::in);}  //metoda otwiera plik
    bool ok(){return file.is_open();}                                     //metoda sprawdza czy plik otwarty
    std::istream& read(){return file;}                                    //metoda czyta dane
    ~FileRead(){file.close();}                                            //metoda zamyka plik
};
#endif // GRAPH_ALGORITHMS_FILEREAD_HPP