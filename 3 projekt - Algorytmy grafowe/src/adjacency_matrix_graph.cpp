#include <istream>
#include <iomanip>
#include <iostream>
#include "graphs/adjacency_matrix_graph.hpp"

AdjacencyMatrixGraph::AdjacencyMatrixGraph(int n, int m)
{
    this->n=n;
    this->m=m;
    this->AdjacencyMatrix.resize(n);
    for(int i=0;i<n;i++)
    {
        this->AdjacencyMatrix[i].resize(n,nullptr);
        this->VertexList.push_back(std::make_shared<vertex>(i,i));
    }
}

std::unique_ptr<Graph> AdjacencyMatrixGraph::createGraph(std::istream& is)
{
    int n, m;
    is>>n>>m;
    AdjacencyMatrixGraph graf = AdjacencyMatrixGraph(n,0);
    int a,b,w;
    for(int i=0;i<m;i++)
    {
        is>>a>>b>>w;
        graf.insertEdge(graf.Vertex_at(a),graf.Vertex_at(b),w);
    }
    return std::make_unique<AdjacencyMatrixGraph>(graf);
}

void AdjacencyMatrixGraph::print_graph()
{
    int i,j;
    std::cout << "     ";
    for( i = 0; i < n; i++ )
        std::cout <<i<<std::setw ( 3 );
    std::cout << std::endl;
    for(  i = 0; i < n ; i++ )
    {
        std::cout << std::setw ( 3 ) << i;
        for(j = 0; j < n; j++)
            std::cout << std::setw(3)<<(AdjacencyMatrix[i][j] == nullptr ? 0:1);
        std::cout << std::endl;
    }
}

std::shared_ptr<vertex> AdjacencyMatrixGraph::insertVertex(int w)
{
    this->VertexList.push_back(std::make_shared<vertex>(w,n++));
    this->AdjacencyMatrix.emplace_back();
    this->AdjacencyMatrix[n-1].resize(n, nullptr);
    for(int i=0;i<n;i++)
    {
        this->AdjacencyMatrix[i].push_back(nullptr);
    }
    return VertexList[n-1];
}

void AdjacencyMatrixGraph::eraseVertex(std::shared_ptr<vertex> v)
{
    if(v == nullptr || VertexList[v->ID] != v)return;
    if(v->ID!=n-1)  // zamieniam miejscami wierzcholek v z ostatnim
    {
        std::swap(VertexList[v->ID], VertexList[n - 1]);         //na liscie wiercholkow
        std::swap(AdjacencyMatrix[v->ID], AdjacencyMatrix[n-1]); //wiersze w macierzy
        for(int c=0;c<n;c++) //zamieniam kolumnami
        {
            std::swap(AdjacencyMatrix[c][v->ID], AdjacencyMatrix[c][n-1]);
        }
        VertexList[v->ID]->ID = v->ID; // Aktualizuje Dane na liscie wierzhcolkow
        VertexList[n-1]->ID = n-1;     // Aktualizuje Dane na liscie wierzhcolkow
    }

    for(int c=0;c<n-1;c++)  //usuwam ostania kolumne
    {
        eraseEdge(AdjacencyMatrix[c][n-1]);
        eraseEdge(AdjacencyMatrix[n-1][c]);
        AdjacencyMatrix[c].pop_back();
    }

    eraseEdge(AdjacencyMatrix[n-1][n-1]);
    AdjacencyMatrix.pop_back();
    VertexList.pop_back();  //usuwam dane z listy wierzhcolkow o usuwanym wierzcholku
    n--;
}

std::shared_ptr<edge> AdjacencyMatrixGraph::insertEdge(std::shared_ptr<vertex> a, std::shared_ptr<vertex> b, int w)
{
    if(a == nullptr || VertexList[a->ID] != a)return nullptr;
    if(b == nullptr || VertexList[b->ID] != b)return nullptr;
    this->EdgeList.push_back(std::make_shared<edge>(a,b,w,m++));
    this->AdjacencyMatrix[a->ID][b->ID]=EdgeList[m-1];
    return EdgeList[m-1];
}

void AdjacencyMatrixGraph::eraseEdge(std::shared_ptr<edge> e)
{
    if(e == nullptr || EdgeList[e->ID] != e)return; //jesli usuwam krawedz ktorej nie ma
    AdjacencyMatrix[e->v->ID][e->w->ID]= nullptr;

    if(e->ID!=m-1)  //zamieniam miejscami w liscie krawedzi miejscami krawedz e z ostatnia
    {
        std::swap(EdgeList[e->ID], EdgeList[m - 1]);
        EdgeList[e->ID]->ID = e->ID; // Aktualizuje dane na liscie krawedzi
    }

    EdgeList.pop_back(); //usuwam dane z listy krawedzi o usuwanej krawedzi
    m--;
}

std::shared_ptr<edge> AdjacencyMatrixGraph::getEdge(std::shared_ptr<vertex> p, std::shared_ptr<vertex> k)
{
    if(p == nullptr || VertexList[p->ID] != p)return nullptr;
    if(k == nullptr || VertexList[k->ID] != k)return nullptr;
    return AdjacencyMatrix[p->ID][k->ID];
}