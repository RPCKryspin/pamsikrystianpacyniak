#include <istream>
#include <algorithm>
#include <iomanip>
#include <iostream>
#include "graphs/adjacency_list_graph.hpp"

AdjacencyListGraph::AdjacencyListGraph(int _n, int _m)
{
    this->n=_n;
    this->m=_m;
    this->AdjacencyList.resize(n);
    for(int i=0;i<n;i++)
        this->VertexList.push_back(std::make_shared<vertex>(vertex(i,i)));
}

std::unique_ptr<Graph> AdjacencyListGraph::createGraph(std::istream& is)
{
    int n, m;
    is>>n>>m;
    AdjacencyListGraph graf = AdjacencyListGraph(n,0);
    int a,b,w;
    for(int i=0;i<m;i++)
    {
        is>>a>>b>>w;
        graf.insertEdge(graf.Vertex_at(a),graf.Vertex_at(b),w);
    }
    return std::make_unique<AdjacencyListGraph>(graf);
}

void AdjacencyListGraph::print_graph()
{
    int i,j;
    for(  i = 0; i < n ; i++ )
    {
        std::cout << "LS [" << i << "] =";
        auto p = AdjacencyList[i].size();
        for(  j = 0; j < p; j++ )
        {
            std::cout << std::setw(2) << AdjacencyList[i][j]->w->ID<< "{"<<AdjacencyList[i][j]->weight<<"}";
            if(j < p-1){std::cout <<",";}
        }
        std::cout << std::endl;
    }
}

std::shared_ptr<vertex> AdjacencyListGraph::insertVertex(int w)
{
    this->VertexList.push_back(std::make_shared<vertex>(w,n++));
    this->AdjacencyList.emplace_back();
    return VertexList[n-1];
}

void AdjacencyListGraph::eraseVertex(std::shared_ptr<vertex> v)
{
    if(v == nullptr || VertexList[v->ID] != v)return;
    for(int c=m-1;c>=0;c--)     //usuwam krawedzie zwiazane z usuwanym wierzcholkiem
    {
        if(EdgeList[c]->v==v || EdgeList[c]->w==v){eraseEdge(EdgeList[c]);}
    }

    if(v->ID!=n-1)  //zamieniam miejscami wierzcholek v z ostatnim
    {
        std::swap(VertexList[v->ID], VertexList[n - 1]);     //na liscie wierzcholkow
        std::swap(AdjacencyList[v->ID], AdjacencyList[n-1]); //na liscie sasiedztwa
        VertexList[v->ID]->ID = v->ID;                              // Aktualizuje dane
        VertexList[n-1]->ID = n-1;                                  // Aktualizuje dane
    }

    AdjacencyList.pop_back();
    VertexList.pop_back(); //usuwam dane z listy o usuwanym wierzcholku
    n--;
}

std::shared_ptr<edge> AdjacencyListGraph::insertEdge(std::shared_ptr<vertex> a, std::shared_ptr<vertex> b, int w)
{
    if(a == nullptr || VertexList[a->ID] != a)return nullptr;
    if(b == nullptr || VertexList[b->ID] != b)return nullptr;
    this->EdgeList.push_back(std::make_shared<edge>(a,b,w,m++));
    this->AdjacencyList[a->ID].push_back(EdgeList[m-1]);
    return EdgeList[m-1];
}

void AdjacencyListGraph::eraseEdge(std::shared_ptr<edge> e)
{
    if(e == nullptr || EdgeList[e->ID] != e)return;   //jesli usuwam krawedz ktora nie ma zakonczenia
    auto id = std::find(AdjacencyList[e->v->ID].begin(),
                        AdjacencyList[e->v->ID].end(), e)
              - AdjacencyList[e->v->ID].begin();
    size_t s = AdjacencyList[e->v->ID].size();

    if(id==s){return;}
    std::swap(AdjacencyList[e->v->ID][id], AdjacencyList[e->v->ID][s-1]);
    AdjacencyList[e->v->ID].pop_back();

    if(e->ID!=m-1) //zamieniam miejscami w liscie krawedzi miejscami krawedz e z ostatnia
    {
        std::swap(EdgeList[e->ID], EdgeList[m - 1]);
        EdgeList[e->ID]->ID = e->ID; // Aktualizuje dane
    }

    EdgeList.pop_back(); //usuwam dane z listy krawedzi o usuwanej krawedzi
    m--;
}

std::shared_ptr<edge> AdjacencyListGraph::getEdge(std::shared_ptr<vertex> p, std::shared_ptr<vertex> k)
{
    if(p == nullptr || VertexList[p->ID] != p)return nullptr;
    if(k == nullptr || VertexList[k->ID] != k)return nullptr;
    std::shared_ptr<edge> r;
    for(const auto& e:AdjacencyList[p->ID]){
        if(e->w == k){
            r = e;
            break;
        }
    }
    return r;
}