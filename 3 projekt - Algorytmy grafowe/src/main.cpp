#include <iostream>
#include "graphs/adjacency_list_graph.hpp"
#include "graphs/adjacency_matrix_graph.hpp"
#include "graphs/fileread.hpp"

int main()
{
    std::unique_ptr<Graph> graph;
    std::cout << std::endl << "Algorytmy Grafowe - Menu Badan Operacji na Grafach Skierowanych " << std::endl;
    std::cout << "Wybierz jaka chcesz przeprowadzic operacje"<<std::endl;
    std::cout << "Pamietaj ze graf zostanie zrestartowany po pomnownym wybraniu reprezentacji" << std::endl<<std::endl;
    std::cout << "Uwaga w pierwszej kolejnosci nalezy wybrac reprezentacje grafu ze wczytaniem gotowego pliku" ;
    std::cout<<std::endl<<std::endl;
    int p1;
    int p2;
    int k1;
    int k2;
    int w;
    int ID1;
    int waga;
    std::shared_ptr<vertex> verp1;
    std::shared_ptr<vertex> verk1;
    std::shared_ptr<edge> krawedz;
    std::shared_ptr<vertex> ver;
    FileRead * fileRead;

    while(true)
    {
        std::cout << "Wpisz cyfre jeden aby Wczytac Graf(Macierz sasiedztwa)" << std::endl;
        std::cout << "Wpisz cyfre dwa aby Wczytac Graf(Lista sasiedztwa)" << std::endl;
        std::cout << "Wpisz cyfre trzy aby Wyswietlic Graf" << std::endl;
        std::cout << "Wpisz cyfre cztery aby dodac wierzcholek" << std::endl;
        std::cout << "Wpisz cyfre piec aby usunac wierzcholek" << std::endl;
        std::cout << "Wpisz cyfre szesc aby dodac krawedz" << std::endl;
        std::cout << "Wpisz cyfre siedem aby usunac krawedz" << std::endl;
        std::cout << "Wpisz cyfre osiem aby zakonczyc badania" << std::endl<< std::endl;

        int wybor;
        std::cout << "Wybieram numer:  ";
        std::cin >> wybor;
        while(std::cin.fail() || wybor> 8 || wybor<= 0)
        {
            std::cout << "Nie znam takiej komendy - wprowadz ponownie" << std::endl;
            std::cin.clear();
            std::cin.ignore(1000,'\n');
            std::cout << std::endl << "Wybieram numer:  ";
            std::cin >> wybor;
        }
        std::cout << "Wybrana opcja " <<wybor<<" "<<std::endl;
        if(wybor == 8){std::cout << "Koncze dzialanie programu" << std::endl;break;}

        switch(wybor)
        {
            case 1:
                std::cout << "Tworze macierz sasiedztwa "<<std::endl;
                std::cout << "Legnda:1 to istniejacej krawedzi pomiedzy wierzcholkami, 0 to jej brak"<<std::endl;
                fileRead = new FileRead("InputData.txt");
                if(fileRead->ok())
                    graph = AdjacencyMatrixGraph::createGraph(fileRead->read());
                delete fileRead;
                break;
            case 2:
                std::cout << "Tworze liste sasiedztwa " <<std::endl;
                std::cout << "Legnda:ID{x}, gdzie x to waga krawedzi skierowanej do wierzolka o ID"<<std::endl;
                fileRead = new FileRead("InputData.txt");
                if(fileRead->ok())
                    graph = AdjacencyListGraph::createGraph(fileRead->read());
                delete fileRead;
                break;
            case 3:
                std::cout<<std::endl;
                graph->print_graph();
                break;
            case 4:
                std::cout << "Wpisz wartosc liczbowa elementu nowego wierzcholka" << std::endl;
                std::cin>>w;
                graph->insertVertex(w);
                break;
            case 5:
                std::cout << "Wpisz ID wierzcholka do usuniecia" << std::endl;
                std::cin>>ID1;
                ver = graph->Vertex_at(ID1);
                graph->eraseVertex(ver);
                break;
            case 6:
                std::cout << "Wpisz wartosc liczbowa wagi krawedzi do dodania" << std::endl;
                std::cin>>waga;
                std::cout << "Wpisz ID wierzcholka poczatkowego krawedzi do dodania " << std::endl;
                std::cin>>p1;
                std::cout << "Wpisz ID wierzcholka koncowego krawedzi do dodania" << std::endl;
                std::cin>>k1;
                verp1= graph->Vertex_at(p1);
                verk1= graph->Vertex_at(k1);
                graph->insertEdge(verp1,verk1,waga);
                break;
            case 7:
                std::cout << "Wpisz ID wierzcholka poczatkowego usuwanej krawedzi" << std::endl;
                std::cin>>p2;
                std::cout << "Wpisz ID wierzcholka koncowego usuwanej krawedzi" << std::endl;
                std::cin>>k2;
                krawedz=graph->getEdge(graph->Vertex_at(p2), graph->Vertex_at(k2));
                graph->eraseEdge(krawedz);
                break;
            default:
                break;
        }
        std::cout << std::endl<< "Zrobione. Co teraz ?" << std::endl<< std::endl;
    }
    return 0;
}
