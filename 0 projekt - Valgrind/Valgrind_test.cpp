#include <iostream>

void memoryLeak() {
    size_t tabSize = 10;
    int *tab = new int[tabSize];
	//delate [] tab;	
}

void readingFromNullptr() {
    int *p = nullptr;
	// int l=0; *p=&l;
    int l = *p;	
}

void uninitializedCondition() {
    int l; // l=0;
    if (l > 0) {
        int i = 0;
    }
}

void invalidWrite() {
    size_t tabSize = 10;
    int *tab = new int[tabSize];
    for (int i = 0; i < tabSize + 1/*-1*/; ++i) 
        tab[i] = i; 
    delete[] tab;
}

void invalidFree() {
    size_t tabSize = 10;
    int *tab = new int[tabSize];
    delete[] tab;
    delete[] tab; // delate line
}

int main() {
    char opcja;
    std::cout << "Wybór opcji!" << std::endl;
    std::cin >> opcja;

    switch (opcja) {
        case 'l':
            std::cout << "Przykład wycieku pamięci" << std::endl;
            memoryLeak();
            break;
        case 's':
            std::cout << "Błąd pamięci - odczyt wartości nullptr" << std::endl;
            readingFromNullptr();
            break;
        case 'i':
            std::cout << "Błąd pamięci - nieprawidłowy zapis do pamięci" << std::endl;
            invalidWrite();
            break;
        case 'f':
            std::cout << "Błąd pamięci - niepoprawne zwolnienie obszaru pamięci" << std::endl;
            invalidFree();
            break;
        case 'c':
            std::cout << "Błąd pamięci - warunek zależny od niezainicjalizowanej zmiennej" << std::endl;
            uninitializedCondition();
            break;
        default:
            std::cout << "Niepoprawna opcja" << std::endl;
            break;
    }
    return 0;
}
