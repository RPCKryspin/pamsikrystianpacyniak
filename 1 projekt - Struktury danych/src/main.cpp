#include <chrono>
#include <iostream>
#include <vector>
#include <algorithm>
#include <unistd.h>
#include <cstdlib>
#include <queue>
#include <stack>
#include <list>

#include "data_structures/queue_list.hpp"
#include "data_structures/queue_tab.hpp"
#include "data_structures/stack_list.hpp"
#include "data_structures/stack_tab.hpp"

//using namespace std;

//https://en.cppreference.com/w/cpp/chrono/duration

void Badaj_Queue_List(int n,int Liczba_Eksperymentow)
{
    for (int i = 0; i < Liczba_Eksperymentow; ++i) //Wykonywanie eksperymentów dla liczby operacji
    {
        QueueList<int> Queue_List;
        /*
        Mierzenie czasu trwania jednego eksperymentu.Poniszy fragment wykonywany jest wielokrotnie
        osobno dla każdej kombinacji typu struktury i rodzaju implementacji.
        */

        auto start = std::chrono::system_clock::now(); // Tu wykonaj operację
        for(int j = 0; j < n; j++)
        {
            Queue_List.enqueue(10);
        }
        for(int j = 0; j < n; j++)
        {
            Queue_List.dequeue();
        }
        auto end = std::chrono::system_clock::now();

        std::chrono::duration<double> diff = (end - start) * 1000; // w sekundach a dla ms diff * 1000
        double durationTime = diff.count();

        /*
        Tu zapisz Zmierzony czas zapisz do pliku oraz liczbę badanych operacji.
        Interesuje nas średnia wartość dla badanej Liczby Eksperymentów
        */

        std::cout << durationTime << " ms" << std::endl;
    }
}

void Badaj_Queue_Tab(int n,int Liczba_Eksperymentow)
{
    for (int i = 0; i < Liczba_Eksperymentow; ++i) //Wykonywanie eksperymentów dla liczby operacji
    {
        QueueTab<int> Queue_Tab;
        /*
        Mierzenie czasu trwania jednego eksperymentu.Poniszy fragment wykonywany jest wielokrotnie
        osobno dla każdej kombinacji typu struktury i rodzaju implementacji.
        */

        auto start = std::chrono::system_clock::now(); // Tu wykonaj operację
        for(int j = 0; j < n; j++)
        {
            Queue_Tab.enqueue(10);
        }
        for(int j = 0; j < n; j++)
        {
            Queue_Tab.dequeue();
        }
        auto end = std::chrono::system_clock::now();

        std::chrono::duration<double> diff = (end - start) * 1000; // w sekundach a dla ms diff * 1000
        double durationTime = diff.count();

        /*
        Tu zapisz Zmierzony czas zapisz do pliku oraz liczbę badanych operacji.
        Interesuje nas średnia wartość dla badanej Liczby Eksperymentów
        */

        std::cout << durationTime << " ms" << std::endl;
    }
}

void Badaj_Stack_List(int n,int Liczba_Eksperymentow)
{
    for (int i = 0; i < Liczba_Eksperymentow; ++i) //Wykonywanie eksperymentów dla liczby operacji
    {
        StackList<int> Stack_List;
        /*
        Mierzenie czasu trwania jednego eksperymentu.Poniszy fragment wykonywany jest wielokrotnie
        osobno dla każdej kombinacji typu struktury i rodzaju implementacji.
        */

        auto start = std::chrono::system_clock::now(); // Tu wykonaj operację
        for(int j = 0; j < n; j++)
        {
            Stack_List.push(10);
        }
        for(int j = 0; j < n; j++)
        {
            Stack_List.pop();
        }
        auto end = std::chrono::system_clock::now();

        std::chrono::duration<double> diff = (end - start) * 1000; // w sekundach a dla ms diff * 1000
        double durationTime = diff.count();

        /*
        Tu zapisz Zmierzony czas zapisz do pliku oraz liczbę badanych operacji.
        Interesuje nas średnia wartość dla badanej Liczby Eksperymentów
        */

        std::cout << durationTime << " ms" << std::endl;
    }
}

void Badaj_Stack_Tab(int n,int Liczba_Eksperymentow)
{
    for (int i = 0; i < Liczba_Eksperymentow; ++i) //Wykonywanie eksperymentów dla liczby operacji
    {
        StackTab<int> Stack_Tab;
        /*
        Mierzenie czasu trwania jednego eksperymentu.Poniszy fragment wykonywany jest wielokrotnie
        osobno dla każdej kombinacji typu struktury i rodzaju implementacji.
        */

        auto start = std::chrono::system_clock::now(); // Tu wykonaj operację
        for(int j = 0; j < n; j++)
        {
            Stack_Tab.push(10);
        }
        for(int j = 0; j < n; j++)
        {
            Stack_Tab.pop();
        }
        auto end = std::chrono::system_clock::now();

        std::chrono::duration<double> diff = (end - start) * 1000; // w sekundach a dla ms diff * 1000
        double durationTime = diff.count();

        /*
        Tu zapisz Zmierzony czas zapisz do pliku oraz liczbę badanych operacji.
        Interesuje nas średnia wartość dla badanej Liczby Eksperymentów
        */

        std::cout << durationTime << " ms" << std::endl;
    }
}
void Operacje_Stos_STL()
{	
	std::stack<int> stos;
        std::cout << std::endl << std::endl << " Menu operacji - Stos STL" << std::endl;
        std::cout << "Wybierz jaka chcesz przeprowadzic operacje"<<std::endl<<std::endl<<std::endl;
	while(1)
        {
                std::cout << "Wpisz cyfre jeden aby dodac losowy element na szczyt stosu" << std::endl;
                std::cout << "Wpisz cyfre dwa aby dodac m losowych elementow na szczyt stosu" << std::endl;
                std::cout << "Wpisz cyfre trzy aby wyswietlic element na szczycie stosu" << std::endl;
                std::cout << "Wpisz cyfre cztery aby usunac element ze szcztu stosu" << std::endl;
                std::cout << "Wpisz cyfre piec aby usunac k elementow ze szczytu stosu" << std::endl;
                std::cout << "Wpisz cyfre szesc aby wyswietlic liczbe elementow w stosie" << std::endl;
		std::cout << "Wpisz cyfre siedem aby zakonczyc badania" << std::endl<< std::endl;

                int wyborSTLstos;
                std::cout << "Wybieram numer:  ";
                std::cin >> wyborSTLstos;
                while(std::cin.fail() || wyborSTLstos > 7 || wyborSTLstos <= 0)
                {	
                        std::cout << "Nie znam takiej komendy - wprowadz ponownie" << std::endl;
                        std::cin.clear();
                        std::cin.ignore(1000,'\n');
                        std::cout << std::endl << "Wybieram numer:  ";
                        std::cin >> wyborSTLstos;
                }
	
                switch(wyborSTLstos)
                {
                        case 1:
                            stos.push(rand());
                            break;
                        case 2:
                            unsigned int m;
                            std::cout << std::endl << "Podaj liczbe elementow: ";
                            std::cin >> m;
                            while(std::cin.fail())
                            {
                                std::cin.clear();
                                std::cin.ignore(1000,'\n');
                                std::cout << std::endl << "Blad. Podaj ponownie liczbe elementow: ";
                                std::cin >> m;
                            }
                            for(int i = 0; i < m; ++i)
                                stos.push(rand());
                            break;
                        case 3:
                            std::cout << std::endl << "Element szczytowy: " << stos.top() << std::endl;
                            break;
                        case 4:
                            stos.pop();
                            break;
                        case 5:
                            unsigned int  k;
                            std::cout << std::endl << "Podaj liczbe elementow: ";
                            std::cin >> k;
                            while(std::cin.fail())
                            {
                                std::cin.clear();
                                std::cin.ignore(1000, '\n');
                                std::cout << std::endl << "Blad. Podaj ponownie liczbe elementow: ";
                                std::cin >> k;
                            }
                            if(k > stos.size())
                            {
                                std::cout << std::endl << "ilosc elementow wieksza niz rozmiar stosu!" << std::endl;
                                std::cout << "Usuwam wszystko!" << std::endl;
                                k = stos.size();
                            }
                            for(int i = 0; i < k; ++i)
                                stos.pop();
                            break;
                        case 6:
                            std::cout << std::endl << "Liczba elementow na stosie: " << stos.size() << std::endl;
                            break;
                }
                std::cout << std::endl<< "Zrobione. Co teraz ?" << std::endl<< std::endl;
		if(wyborSTLstos == 7){break;}
        }
}
void Operacje_Kolejka_STL()
{
	std::queue<int> kolejka;
        std::cout << std::endl << std::endl << "Menu operacji - Kolejka STL"<<std::endl;
        std::cout << "Wybierz jaka chcesz przeprowadzic operacje"<<std::endl<<std::endl<<std::endl;
	while(1)
        {
		std::cout << "Wpisz cyfre jeden aby dodac losowy element na koniec kolejki" << std::endl;
                std::cout << "Wpisz cyfre dwa aby dodac l losowych elementow na koniec kolejki" << std::endl;
                std::cout << "Wpisz cyfre trzy aby usunac element z poczatku kolejki" << std::endl;
                std::cout << "Wpisz cyfre cztery aby usunac  k elementow z poczatku kolejki" << std::endl;
                std::cout << "Wpisz cyfre piec aby wyswietlic pierwszy element kolejki" << std::endl;
                std::cout << "Wpisz cyfre szesc aby wyswietlic liczbe elementow w kolejce" << std::endl;
                std::cout << "Wpisz cyfre siedem aby zakonczyc badania" << std::endl<< std::endl;

                int wyborSTLkolejka;
                std::cout << "Wybieram numer:  ";
                std::cin >> wyborSTLkolejka;
                while(std::cin.fail() || wyborSTLkolejka> 7 || wyborSTLkolejka<= 0)
                {
                       std::cout << "Nie znam takiej komendy - wprowadz ponownie" << std::endl;
                       std::cin.clear();
                       std::cin.ignore(1000,'\n');
                       std::cout << std::endl << "Wybieram numer:  ";
                       std::cin >> wyborSTLkolejka;
                }

                switch(wyborSTLkolejka)
                {
                        case 1:
                            kolejka.push(rand());
                            break;
                        case 2:
                            unsigned int m;
                            std::cout << std::endl << "Podaj liczbe elementow: ";
                            std::cin >> m;
                            while(std::cin.fail())
                            {
                                std::cin.clear();
                                std::cin.ignore(1000,'\n');
                                std::cout << std::endl << "Blad. Podaj ponownie liczbe elementow: ";
                                std::cin >> m;
                            }
                            for(int i = 0; i < m; ++i)
                                kolejka.push(rand());
                            break;
                        case 3:
                            kolejka.pop();
                            break;
                        case 4:
                            unsigned int  k;
                            std::cout << std::endl << "Podaj liczbe elementow: ";
                            std::cin >> k;
                            while(std::cin.fail())
                            {
                                std::cin.clear();
                                std::cin.ignore(1000,'\n');
                                std::cout << std::endl << "Blad. Podaj ponownie liczbe elementow: ";
                                std::cin >> k;
                            }
                            if(k > kolejka.size())
                            {
                                std::cout << std::endl << "ilosc elementow wieksza niz rozmiar kolejki!" << std::endl;
                                std::cout << "Usuwam wszystko!" << std::endl;
                                k = kolejka.size();
                            }
                            for(int i = 0; i < k; ++i)
                                kolejka.pop();
                            break;
                        case 5:
                            std::cout << std::endl << "Pierwszy element kolejki: " << kolejka.front() << std::endl;
                            break;
                        case 6:
                            std::cout << std::endl << "Liczba elementow w kolejce: " << kolejka.size() << std::endl;
                            break;    
                }
                std::cout << std::endl<< "Zrobione. Co teraz ?" << std::endl<< std::endl;
		if(wyborSTLkolejka == 7){break;}
	}
}

void Operacje_Lista_STL()
{
	std::list<int> lista;
        std::cout << std::endl << std::endl << "Menu operacji - Lista STL"<<std::endl;
        std::cout << "Wybierz jaka chcesz przeprowadzic operacje"<<std::endl<<std::endl<<std::endl;
	while(1)
        {
                std::cout << "Wpisz cyfre jeden aby wyswietlic zawartosc listy" << std::endl;
                std::cout << "Wpisz cyfre dwa aby wyswietlic liczbe elementow listy" << std::endl;
                std::cout << "Wpisz cyfre trzy aby wyswietlic ostatni element listy" << std::endl;
                std::cout << "Wpisz cyfre cztery aby wyswietlic pierwszy element listy" << std::endl;
                std::cout << "Wpisz cyfre piec aby dodac losowy element na poczatek listy" << std::endl;
                std::cout << "Wpisz cyfre szesc aby dodac losowy element na koniec listy" << std::endl;
                std::cout << "Wpisz cyfre siedem aby dodac k elementow na poczatek listy" << std::endl;
                std::cout << "Wpisz cyfre osiem aby dodac l elementow na koniec listy" << std::endl;
                std::cout << "Wpisz cyfre dziewiec aby usunac element na koncu listy" << std::endl;
                std::cout << "Wpisz cyfre dziesiec aby usunac j elemntow na koncu listy" << std::endl;
                std::cout << "Wpisz cyfre jedenascie aby usunac wszystkie elementy z listy" << std::endl;
                std::cout << "Wpisz cyfre dwanascie aby zakonczyc badania" << std::endl<< std::endl;

                int wyborSTLlista;
                std::cout << "Wybieram numer:  ";
                std::cin >> wyborSTLlista;
		while(std::cin.fail() || wyborSTLlista> 12 || wyborSTLlista<= 0)
                {
                	std::cout << "Nie znam takiej komendy - wprowadz ponownie" << std::endl;
                        std::cin.clear();
                        std::cin.ignore(1000,'\n');
                        std::cout << std::endl << "Wybieram numer:  ";
                        std::cin >> wyborSTLlista;
                }

                 switch(wyborSTLlista)
                 {	
                 	case 1:
                            if(lista.size() == 0)
                                std::cout << std::endl << "Pusta Lista" << std::endl;
                            else
                                std::cout << std::endl << "Elementy Listy: ";
                            for(int ElemTab:lista)
                                std::cout << ElemTab << ", ";
                            std::cout << std::endl;
                            break;
                        case 2:
                            std::cout << std::endl << "Liczba elementow listy: " << lista.size() << std::endl;
                            break;
                        case 3:
                            std::cout << std::endl << "Ostatni element listy: " << lista.back() << std::endl;
                            break;
                        case 4:
                            std::cout << std::endl << "Pierwszy element listy: " << lista.front() << std::endl;
                            break;
                        case 5:
                            lista.push_front(rand());
                            break;
                        case 6:
                            lista.push_back(rand());
                            break;
                        case 7:
                            int k;
                            std::cout << std::endl << "Podaj liczbe elementow: ";
                            std::cin >> k;
                            while(std::cin.fail())
                            {
                                std::cin.clear();
                                std::cin.ignore(1000,'\n');
                                std::cout << std::endl << "Blad. Podaj ponownie liczbe elementow: ";
                                std::cin >> k;
                            }
                            for(int i = 0; i < k; ++i)
                                lista.push_front(rand());
                            break;
                        case 8:
                            int l;
                            std::cout << std::endl << "Podaj liczbe elementow: ";
                            std::cin >> l;
                            while(std::cin.fail())
                            {
                                std::cin.clear();
                                std::cin.ignore(1000,'\n');
                                std::cout << std::endl << "Blad. Podaj ponownie liczbe elementow: ";
                                std::cin >> l;
                            }
                            for(int i = 0; i < l; ++i)
                                lista.push_back(rand());
                            break;
                        case 9:
                            lista.pop_back();
                            break;
                        case 10:
                            int j;
                            std::cout << std::endl << "Podaj liczbe elementow: ";
                            std::cin >> j;
                            while(std::cin.fail())
                            {
                                std::cin.clear();
                                std::cin.ignore(1000,'\n');
                                std::cout << std::endl << "Blad. Podaj ponownie liczbe elementow: ";
                                std::cin >> j;
                            }
                            if(j > lista.size())
                            {
                                std::cout << std::endl << "ilosc elementow wieksza niz rozmiar listy!" << std::endl;
                                std::cout << "Usuwam wszystko!" << std::endl;
                                j = lista.size();
                            }
                            for(int i = 0; i < j; ++i)
                                lista.pop_back();
                            break;
                        case 11:
                            lista.clear();
                            break;
                 }
            std::cout << std::endl<< "Zrobione. Co teraz ?" << std::endl<< std::endl;
                 if(wyborSTLlista == 12){break;}
	}
}

void Kontenery()
{
    std::cout << "Struktury danych - Menu Badan operacji Kontenerow STL "<< std::endl;
    std::cout << "Program zostanie autmatycznie zakonczony po zakonczeniu badan wybranej struktury" << std::endl;
    std::cout << "Wybierz jaki chcesz sprawdzic kontener"<<std::endl<<std::endl<<std::endl;
    std::cout << "Wpisz cyfre jeden jesli Stos" << std::endl;
    std::cout << "Wpisz cyfre dwa jesli Kolejka" << std::endl;
    std::cout << "Wpisz cyfre trzy jesli Lista" << std::endl;
    std::cout << "Wpisz cyfre cztery aby wyjsc z programu" << std::endl;

    int wyborSTL;
    std::cout << "Wybieram numer:  ";
    std::cin >> wyborSTL;
    while(std::cin.fail() || wyborSTL> 4 || wyborSTL<= 0)
    {
        std::cout << "Nie znam takiego kontenera - wprowadz ponownie" << std::endl;
        std::cin.clear();
        std::cin.ignore(1000,'\n');
        std::cout << std::endl << "Wybieram numer:  ";
        std::cin >> wyborSTL;
    }

    switch(wyborSTL)
    {
        case 1:
            Operacje_Stos_STL();
            break;
        case 2:
            Operacje_Kolejka_STL();
            break;
        case 3:
            Operacje_Lista_STL();
            break;
        case 4:
            std::cout << std::endl << "Koncze dzialanie programu" << std::endl;
            break;
    }
}

int main(int argc, char* argv[])
{
    std::vector<int> Vector_ilosci_operacji{10, 100, 1000, 10000, 100000};
    const int Liczba_Eksperymentow = 100; // Wybierz liczbe pomiarow do wysterowania procesem

    std::cout << "Struktury danych - Menu Badan Zlozonosci Czasowej " << std::endl << std::endl;
    std::cout << "Program zostanie autmatycznie zakonczony po przeprowadzeniu badania wybranej struktury" << std::endl;
    std::cout << "Wybierz jaka chcesz zbadac strukture" << std::endl << std::endl;
    std::cout << "Wpisz cyfre jeden jesli Kolejka - implementancja listowa" << std::endl;
    std::cout << "Wpisz cyfre dwa jesli Kolejka - implementancja tablicowa cykliczna" << std::endl;
    std::cout << "Wpisz cyfre trzy jesli Stos - implementancja listowa" << std::endl;
    std::cout << "Wpisz cyfre cztery jesli Stos - implementancja tablicowa" << std::endl;
    std::cout << "Wpisz cyfre piec aby przejsc do Menu Badan operacji Kontenerow STL " << std::endl;
    std::cout << "Wpisz cyfre szesc aby wyjsc z programu" << std::endl;

    int wybor;
    std::cout << std::endl << "Wybieram numer:  ";
    std::cin >> wybor;
    while(std::cin.fail() || wybor > 6 || wybor <= 0)
    {
        std::cout << "Nie znam takiego polecenia - wprowadz ponownie" << std::endl;
        std::cin.clear();
        std::cin.ignore(1000, '\n');
        std::cout << std::endl << "Wybieram numer:  ";
        std::cin >> wybor;
    }

    switch(wybor)
    {
        case 1:
            for(const auto& n : Vector_ilosci_operacji) // Badanie dla różnej liczby operacji
            {
                std::cout << "Badam serie " << n << " operacji" << std::endl;
                Badaj_Queue_List(n, Liczba_Eksperymentow);
            }
            break;
        case 2:
            for(const auto& n : Vector_ilosci_operacji) // Badanie dla różnej liczby operacji
            {
                std::cout << "Badam serie " << n << " operacji" << std::endl;
                Badaj_Queue_Tab(n, Liczba_Eksperymentow);
            }
            break;
        case 3:
            for(const auto& n : Vector_ilosci_operacji) // Badanie dla różnej liczby operacji
            {
                std::cout << "Badam serie " << n << " operacji" << std::endl;
                Badaj_Stack_List(n, Liczba_Eksperymentow);
            }
            break;
        case 4:
            for(const auto& n : Vector_ilosci_operacji) // Badanie dla różnej liczby operacji
            {
                std::cout << "Badam serie " << n << " operacji" << std::endl;
                Badaj_Stack_Tab(n, Liczba_Eksperymentow);
            }
            break;
        case 5:
            Kontenery();
            break;
        case 6:
            std::cout << std::endl << "Koncze dzialanie programu" << std::endl;
            break;
    }
    return 0;
}
