#ifndef DATA_STRUCTURES_QUEUE_LIST_HPP
#define DATA_STRUCTURES_QUEUE_LIST_HPP
#include "doubly_linked_list.hpp"

template <typename T>
class QueueList             //  Klasa Kolejka FIFO z implementacją listowa
{
    DoublyLinkedList<T> glowna_lista;   // wskaznik na pierwszy element listy
  public:
    ~QueueList(){};               //Destruktor Kolejki
    QueueList();                  // Konstruktor kolejki
    void enqueue(const T& newElement);    // metoda dodaje element na koniec kolejki
    T dequeue();                          // metoda usuwa pierwszy element z kolejki i zwraca go
    bool empty();                         // metoda sprawdza czy tablica zawiera elementy
    std::size_t size();                   // metoda sprawdza rozmiar kolejki w zajmowanej tablicy (ilosc elementow)
    const T& front();                     // metoda zwraca wartosc elementu z przodu bez usuwania go
};

template <typename T>
QueueList<T>::QueueList()
{
    DoublyLinkedList<T> glowna_lista;
}

template <typename T>
bool QueueList<T>::empty()
{
    return glowna_lista.empty();
}

template <typename T>
std::size_t QueueList<T>::size()
{
   return glowna_lista.size();
}

template <typename T>
const T& QueueList<T>::front()
{
    return glowna_lista.front();
}


template <typename T>
void QueueList<T>::enqueue(const T& newElement)
{
    glowna_lista.addBack(newElement);
}

template <typename T>
T QueueList<T>::dequeue()
{
    T zwroc = front();
    glowna_lista.removeFront();
    return zwroc;
}


#endif // DATA_STRUCTURES_QUEUE_LIST_HPP
