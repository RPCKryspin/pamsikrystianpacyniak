#ifndef DATA_STRUCTURES_HEAP_HPP
#define DATA_STRUCTURES_HEAP_HPP
#include<stdexcept>

template <typename T>
class Heap 				// Klasa kopiec binarny typu MAX implementacja tablicowa
{
    T * vector;                         // wskaznik na element tablicy kopca
    std::size_t origin_size;            // rozmiar tablicy
    std::size_t nr_elements;            // liczba elementow
    void resize(std::size_t new_size);  // metoda do zmiany rozmairu tablicy
  public:
    ~Heap(){delete[] vector;};          // Destruktor
    Heap();                             // Konstruktor bezparametryczny Kopca
    void insert(const T& newElement);   // metoda wstawia element do kopca
    void removeMax();                   // metoda usuwa element szczytowy(korzen) z kopca
    std::size_t size();                 // metoda zwraca ilosc elementow kopca
    bool empty();                       // metoda sprawdza czy kopiec ma elementy
    T& operator[](int index);           // przeciazenie operatora kopca dla elementow tablicy kopca
    T getMax();                         // Metoda zwraca element Maxymalny
};

template <typename T>
Heap<T>::Heap()
{
    vector = new T[2]; // Dla ułatwienia przyjmujemy indeksy od 1 // Parzysty syn lewy, nieparzysty prawy
    origin_size = 2;
    nr_elements = 0;
}
template <typename T>
T Heap<T>::getMax()
{
    return vector[1];
}
template <typename T>
void Heap<T>::insert(const T& newElement)
{
    if( nr_elements+1 == origin_size)     //rezerwacja przestrzeni tablicy
    {
        resize(origin_size*2);
    }

    vector[++nr_elements] = newElement; // insercja

    int i = nr_elements;
    while(i>1 and vector[i]> vector[i/2]  ) // kopcowatosc
    {
        T temp = vector[i/2];
        vector[i/2]=vector[i];
        vector[i]=temp;
        i=i/2;
    }
}

template <typename T>
void Heap<T>::removeMax()
{
    ~vector[1]; // remove max and transfer last element
    vector[1]=vector[nr_elements];
    ~vector[nr_elements];
    nr_elements--;

    int i=1;
    while(i<=nr_elements/2 and (vector[i]< vector[2*i] or vector[i]< vector[2*i+1]) ) // kopcowatosc
    {
        if(vector[2*i+1]< vector[2*i])
        {
            T temp = vector[2*i];
            vector[2*i]=vector[i];
            vector[i]=temp;
            i=2*i;
        }
        else
        {
            T temp = vector[2*i+1];
            vector[2*i+1]=vector[i];
            vector[i]=temp;
            i=2*i+1;
        }
    }
}

template <typename T>
std::size_t Heap<T>::size()
{
    return nr_elements;
}

template <typename T>
bool Heap<T>::empty()
{
    return nr_elements==0;
}

template <typename T>
T& Heap<T>::operator[](int index)
{
    if(index == 0)
    {
        throw std::invalid_argument("no_zero_element_in_heap");
    }
    else if(index > nr_elements)
    {
        throw std::invalid_argument("no_index_element_in_heap_(out_of_range)");
    }
    else
    {
        return vector[index];
    }
}
template <typename T>
void Heap<T>::resize(std::size_t new_size)
{
    T * new_vector = new T[new_size];
    for(int i=1; i<new_size && i<origin_size;i++)
    {
        new_vector[i] = vector[i];
    }
    delete[] vector;
    vector = new_vector;
    origin_size=new_size;
}
#endif // DATA_STRUCTURES_HEAP_HPP

