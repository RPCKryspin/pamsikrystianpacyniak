#ifndef DATA_STRUCTURES_STACK_LIST_HPP
#define DATA_STRUCTURES_STACK_LIST_HPP
#include "singly_linked_list.hpp"

template <typename T>
class StackList             //  Klasa Stos LIFO z implementacją listową
{
    SinglyLinkedList<T> glowna_lista;   // wskaznik na pierwszy element listy
  public:
    ~StackList(){};           // Destruktor stosu
    StackList();            // Konstruktor stosu
    void push(const T& newElement);             // metoda dodaje element na szczyt
    T pop();                                    // metoda usuwa i zwraca element ze szczytu
    bool empty();                               // metoda sprawdza czy tablica zawiera elementy
    std::size_t size();                         // metoda sprawdza rozmiar stosu w zajmowanej tablicy (ilosc elementow)
    const T& top();                             // metoda zwraca wartosc elementu szczytowego bez usuwania go
};


template <typename T>
StackList<T>::StackList() // konstruktor z listy
{
    SinglyLinkedList<T> glowna_lista;
}
template <typename T>
bool StackList<T>::empty() // metoda empty z listy
{
    return glowna_lista.empty();
}

template <typename T>
std::size_t StackList<T>::size() // metoda size z listy
{
    return glowna_lista.size();
}

template <typename T>
void StackList<T>::push(const T& newElement) // metoda addfront z listy
{
    glowna_lista.addFront(newElement);
}

template <typename T>
const T& StackList<T>::top() // meotda front z listy
{
    return glowna_lista.front();
}

template <typename T>
T StackList<T>::pop() // metoda back i removeback z listy
{
    T pop_element = top();
    glowna_lista.removeFront();
    return pop_element;
}


#endif // DATA_STRUCTURES_STACK_LIST_HPP
