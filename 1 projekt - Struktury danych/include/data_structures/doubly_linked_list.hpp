#ifndef DATA_STRUCTURES_DOUBLY_LINKED_LIST_HPP
#define DATA_STRUCTURES_DOUBLY_LINKED_LIST_HPP
#include<stdexcept>

template <typename T>
class DNode //klasa pomocnicza wezel listy dwukierunkowej
{
    T element;                           // wartosc elementu listy
    DNode<T>* next;                      // nastepny element listy
    DNode<T>* prev;                      // poprzedni element listy
  public:
    ~DNode(){};                                     // Destruktor
    DNode(){next=nullptr;prev=nullptr;};                          //konstruktor bezparametryczny
    DNode(T newE){next=nullptr;prev=nullptr;element = newE;};     //konstruktor parametryczny
    T& getElement() {return element;};               //metody dostepu do danych:
    DNode<T>* getNext() {return next;};              //metody dostepu do danych:
    DNode<T>* getPrev() {return prev;};              //metody dostepu do danych:
    void setElement(T newE) {element = newE;} ;      //metody modyfikujace
    void setNext(DNode<T>* newN) {next = newN;};     //metody modyfikujace
    void setPrev(DNode<T>* newN) {prev = newN;};     //metody modyfikujace
};

template <typename T>
class DoublyLinkedList  //klasa lista dwukierunkowa
{
    std::size_t nr_nodes;               // rozmiar listy (liczba wezlow/elementow)
    DNode<T> * head;                    // wskaznik na pierwszy element listy
    DNode<T> * tail;                    // wskaznik na ostatni element listy
  public:
    ~DoublyLinkedList();                // Destruktor
    DoublyLinkedList();                 // konstruktor bezparametryczny listy
    bool empty();                       // metoda sprawdza czy lista jest pusta
    std::size_t size();                 // metoda sprawdza liczbe elementow listy
    void addFront(const T& newElement); // metoda dodaje element na poczatek listy
    void removeFront();                 // metoda usuwa element z poczatku listy
    void addBack(const T& newElement);  // metoda dodaje element na koniec listy
    void removeBack();                  // metoda usuwa element z konca listy
    const T& front() const;             // metoda zwraca wartosc pierwszego elementu
    const T& back() const;              // metoda zwraca wartosc ostatniego elementu
    void insert(const T& newElement, int index);   // metoda dodaje element przed zadany lement
    void remove(const T& element);                 // metoda usuwa dany element
    T& operator[](int index);                      // przeciazenie operatora indeksowania
};

template <typename T>
DoublyLinkedList<T>::DoublyLinkedList()
{
    nr_nodes = 0;
    head = nullptr;
    tail = nullptr;
}

template <typename T>
bool DoublyLinkedList<T>::empty()
{
    return head==nullptr;
}

template <typename T>
DoublyLinkedList<T>::~DoublyLinkedList()
{
   while(!empty())
   {
       removeFront();
   }
}

template <typename T>
std::size_t DoublyLinkedList<T>::size()
{
    return nr_nodes;
}

template <typename T>
void DoublyLinkedList<T>::addFront(const T& newElement)
{
    if (head==nullptr)
    {
        head = new DNode<T>;
        tail = head;
        head->setElement(newElement);
        nr_nodes++;
    }
    else
    {
        DNode<T>* temp = head;
        head = new DNode<T>;
        head->setElement(newElement);
        head->setNext(temp);
        temp->setPrev(head);
        nr_nodes++;
    }
}

template <typename T>
void DoublyLinkedList<T>::removeFront()
{
    if(head == nullptr)
    {
        return;
    }
    if (head->getNext()!=nullptr)
    {
        DNode<T>* temp = head->getNext();
        temp->setPrev(nullptr);
        delete head;
        head = temp;
        nr_nodes--;
    }
    else
    {
        delete head;
        head = nullptr;
        tail = nullptr;
        nr_nodes--;
    }
}
template <typename T>
void DoublyLinkedList<T>::addBack(const T& newElement)
{
    if(head == nullptr)
    {
        addFront(newElement);
    }
    else
    {
        DNode<T>* temp = tail;
        tail = new DNode<T>;
        tail->setElement(newElement);
        tail->setPrev(temp);
        temp->setNext(tail);
        nr_nodes++;
    }
}
template <typename T>
void DoublyLinkedList<T>::removeBack()
{
    if(head == nullptr)
    {
        return;
    }
    if(head->getNext() == nullptr)
    {
        delete head;
        head = nullptr;
        tail = nullptr;
        nr_nodes--;
    }
    else
    {
        DNode<T>* temp = tail->getPrev();
        delete tail;
        temp->setNext(nullptr);
        tail=temp;
        nr_nodes--;
    }
}
template <typename T>
const T& DoublyLinkedList<T>::front() const
{
    return head->getElement();
}

template <typename T>
const T& DoublyLinkedList<T>::back() const
{
    return tail->getElement();
}
template <typename T>
void DoublyLinkedList<T>::insert(const T& newElement, int index)
{
    if(index==0)
    {
        addFront(newElement);
    }
    else if (index+1 > nr_nodes)
    {
        throw std::invalid_argument( "bad_index_insert" );
    }
    else
    {
        DNode<T>* temp = head;
        for(int i = 0; i < index - 1; i++)
        {
            temp = temp->getNext();
        }
        DNode<T>* temp2 = temp->getNext();
        DNode<T>* temp_new = new DNode<T>;
        temp_new->setElement(newElement);

        temp_new->setNext(temp2);
        temp2->setPrev(temp_new);

        temp->setNext(temp_new);
        temp_new->setPrev(temp);
        nr_nodes++;
    }
}
template <typename T>
void DoublyLinkedList<T>::remove(const T& element)
{
    if(head==nullptr){return;}
    if(head->getNext()==nullptr)
    {
        if(head->getElement()==element){removeFront();}
    }
    else
    {
        DNode<T>* temp = head;
        while( temp->getNext()->getNext() != nullptr )
        {
            if(temp->getNext()->getElement() == element)
            {
                DNode<T>* temp2 = temp->getNext()->getNext();
                delete temp->getNext();
                temp->setNext(temp2);
                temp2->setPrev(temp);
                nr_nodes--;
            }
            else
            {
                temp = temp->getNext();
            }
        }
        if(temp->getNext()->getElement() == element)
        {
            delete temp->getNext();
            temp->setNext(nullptr);
            nr_nodes--;
        }
        if(head->getElement()==element){removeFront();}
    }
}

template <typename T>
T& DoublyLinkedList<T>::operator[](int index)
{
    if(index+1 > nr_nodes)
    {
        throw std::invalid_argument( "bad_index_out_of_range" );
    }

    DNode<T>* temp = head;
    for(int i =0;i<index;i++)
    {
        temp = temp->getNext();
    }
    return temp->getElement();
}

#endif // DATA_STRUCTURES_DOUBLY_LINKED_LIST_HPP
