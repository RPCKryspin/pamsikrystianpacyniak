#ifndef QUEUE_HPP_
#define QUEUE_HPP_

template <typename T>
class QueueTab  // klasa kolejka FIFO implementacja tablicowo cykliczna
{
    T * vector;                 // wskaznik na element tablicy kolejki
    std::size_t origin_size;    // rozmiar tablicy
    std::size_t nr_last;       // liczba elementow
    std::size_t nr_first;      // liczba elementow
    std::size_t min_capacity; // minimalny rozmiar tablicy
    void resize(std::size_t new_size); // metoda zmieniająca rozmiar tablicy
  public:
    ~QueueTab(){delete [] vector;};                 // Destruktor
    QueueTab();                                     // konstruktor bezparametryczny tworzy kolejke dla 1 elementu
    explicit QueueTab(std::size_t initialCapacity); // konstruktor inicjalizuje kolejke z zarezerwowanym rozmairem tablicy
    void enqueue(const T& newElement);    // metoda dodaje element na koniec kolejki
    T dequeue();                          // metoda usuwa pierwszy element z kolejki i zwraca go
    bool empty();                         // metoda sprawdza czy tablica zawiera elementy
    std::size_t size();                   // metoda sprawdza rozmiar kolejki w zajmowanej tablicy (ilosc elementow)
    const T& front();                     // metoda zwraca wartosc elementu z przodu bez usuwania go
};

template <typename T>
QueueTab<T>::QueueTab()
{
    vector = new T[1];
    origin_size = 1;
    nr_last = nr_first = 0;
    min_capacity = 1;
}

template <typename T>
QueueTab<T>::QueueTab(std::size_t initialCapacity)
{
    vector = new T[initialCapacity];
    origin_size = initialCapacity;
    min_capacity = initialCapacity;
    nr_last = nr_first = 0;
}

template <typename T>
void QueueTab<T>::enqueue(const T& newElement)
{
    if( size()==origin_size-1)
    {
        resize(origin_size*2);
    }
    vector[nr_last++] = newElement;
    nr_last %= origin_size;

}

template <typename T>
T QueueTab<T>::dequeue()
{
    if (size() <= origin_size/4 && origin_size >= min_capacity*2)
        // Chcemy zmiejszyc ilosc uzywanej pamieci wzgledem operacji
    {
        resize(origin_size/2);
    }
    T first_element = front();
    ~vector[nr_first];
    nr_first++;
    nr_first %= origin_size;
    return first_element;
}

template <typename T>
bool QueueTab<T>::empty()
{
    return nr_first==nr_last ;
}

template <typename T>
std::size_t QueueTab<T>::size()
{

    return (origin_size-nr_first+nr_last)%origin_size;
}

template <typename T>
const T& QueueTab<T>::front()
{
    return vector[nr_first];
}
template <typename T>
void QueueTab<T>::resize(std::size_t new_size)
{
    T * new_vector = new T[new_size];
    for(int i=0; i<new_size && i<origin_size;i++)
    {
        new_vector[i] = vector[(i+nr_first)%origin_size];
    }
    delete[] vector;
    vector = new_vector;
    nr_last=size();
    nr_first=0;
    origin_size=new_size;
}

#endif /* QUEUE_HPP_ */
