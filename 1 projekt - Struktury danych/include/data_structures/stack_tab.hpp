#ifndef STACK_HPP_
#define STACK_HPP_

template <typename T>
class StackTab //klasa stos LIFO implementacja tablicowa
{
    T * vector;                         // wskaznik na element tablicy stosu
    std::size_t origin_size;            // rozmiar tablicy
    unsigned int nr_elements;           // liczba elementow (element szczytowy)
    void resize(std::size_t new_size);  // metoda do zmiany rozmairu tablicy (przepisywania)
  public:
    ~StackTab(){delete [] vector;};                // Destruktor
    StackTab();                                    // konstruktor bezparametryczny tworzy stos dla 1 elementu
    explicit StackTab(std::size_t initialCapacity);// konstruktor inicjalizuje stos z zarezerwowanym rozmairem tablicy
    void push(const T& newElement);             // metoda dodaje element na szczyt
    T pop();                                    // metoda usuwa i zwraca element ze szczytu
    bool empty();                               // metoda sprawdza czy tablica zawiera elementy
    std::size_t size();                         // metoda sprawdza rozmiar stosu w zajmowanej tablicy (ilosc elementow)
    const T& top();                             // metoda zwraca wartosc elementu szczytowego bez usuwania go
};

template <typename T>
StackTab<T>::StackTab()
{
    vector = new T[1];
    origin_size = 1;
    nr_elements = 0;
}

template <typename T>
StackTab<T>::StackTab(std::size_t initialCapacity)
{
    vector = new T[initialCapacity];
    origin_size = initialCapacity;
    nr_elements = 0;
}

template <typename T>
void StackTab<T>::push(const T& newElement)
{
    if( (size_t)nr_elements == origin_size)
        // rezerwacja przestrzeni tablicy
    {
        resize(origin_size*2);
    }
    vector[nr_elements++] = newElement;
}

template <typename T>
T StackTab<T>::pop()
{
    T pop_element = top();
    ~vector[--nr_elements];
    if(nr_elements <= origin_size / 4 && nr_elements > 0)
        // Chcemy zmiejszyc ilosc uzywanej pamieci wzgledem operacji
    {
        resize(origin_size / 2);
    }
    return pop_element;
}

template <typename T>
bool StackTab<T>::empty()
{
    return nr_elements==0;
}

template <typename T>
std::size_t StackTab<T>::size()
{
    return (size_t)nr_elements;
}

template <typename T>
const T& StackTab<T>::top()
{
    return vector[(nr_elements-1)];
}
template <typename T>
void StackTab<T>::resize(std::size_t new_size)
{
    T * new_vector = new T[new_size];
    for(int i=0; i<new_size && i<origin_size;i++)
    {
        new_vector[i] = vector[i];
    }
    delete[] vector;
    vector = new_vector;
    origin_size=new_size;
}

#endif /* STACK_HPP_ */