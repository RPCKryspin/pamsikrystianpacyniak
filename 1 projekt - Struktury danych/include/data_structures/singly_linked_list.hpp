#ifndef DATA_STRUCTURES_SINGLY_LINKED_LIST_HPP
#define DATA_STRUCTURES_SINGLY_LINKED_LIST_HPP
#include<stdexcept>


template <typename T>
class SNode //klasa pomocnicza wezel listy jednokierunkowej
{
    T element;                           // wartosc elementu listy
    SNode<T>* next;                      // nastepny element listy
  public:
    ~SNode(){};                                     // Destruktor
    SNode(){next=nullptr;};                          //konstruktor bezparametryczny
    SNode(T newE){next=nullptr;element = newE;};     //konstruktor parametryczny
    T& getElement() {return element;};               //metody dostepu do danych:
    SNode<T>* getNext() {return next;};              //metody dostepu do danych:
    void setElement(T newE) {element = newE;} ;      //metody modyfikujace
    void setNext(SNode<T>* newN) {next = newN;};     //metody modyfikujace
};

template <typename T>
class SinglyLinkedList //klasa lista jednokierunkowa
{
    std::size_t nr_nodes;               // rozmiar listy (liczba wezlow/elementow)
    SNode<T> * head;                    // wskaznik na pierwszy element listy
  public:
    ~SinglyLinkedList();                // Destruktor
    SinglyLinkedList();                 // konstruktor bezparametryczny listy
    bool empty();                       // metoda sprawdza czy lisat jest pusta
    std::size_t size();                 // metoda sprawdza liczbe elementow listy
    void addFront(const T& newElement); // metoda dodaje element na poczatek listy
    void removeFront();                 // metoda usuwa element z poczatku listy
    void addBack(const T& newElement);  // metoda dodaje element na koniec listy
    void removeBack();                  // metoda usuwa element z konca listy
    const T& front() const;             // metoda zwraca wartosc pierwszego elementu
    const T& back() const;              // metoda zwraca wartosc ostatniego elementu
    void insert(const T& newElement, int index);   // metoda dodaje element przed zadany lement
    void remove(const T& element);                 // metoda usuwa dany element
    T& operator[](int index);                      // przeciazenie operatora indeksowania
};
template <typename T>
SinglyLinkedList<T>::SinglyLinkedList()
{
    nr_nodes = 0;
    head = nullptr;
}

template <typename T>
bool SinglyLinkedList<T>::empty()
{
    return head==nullptr;
}

template <typename T>
SinglyLinkedList<T>::~SinglyLinkedList()
{
    while(!empty())
    {
        removeFront();
    }
}

template <typename T>
std::size_t SinglyLinkedList<T>::size()
{
    return nr_nodes;
}

template <typename T>
void SinglyLinkedList<T>::addFront(const T& newElement)
{

    if (head==nullptr)
    {
        head = new SNode<T>;
        head->setElement(newElement);
        nr_nodes++;
    }
    else
    {
        SNode<T>* temp = head;
        head = new SNode<T>;
        head->setElement(newElement);
        head->setNext(temp);
        nr_nodes++;
    }

}
template <typename T>
void SinglyLinkedList<T>::removeFront()
{
    if(head == nullptr)
    {
        return;
    }
    if(head->getNext() == nullptr)
    {
        delete head;
        head = nullptr;
        nr_nodes--;
    }
    else
    {
        SNode<T>* temp = head->getNext();
        delete head;
        head = temp;
        nr_nodes--;
    }
}
template <typename T>
void SinglyLinkedList<T>::addBack(const T& newElement)
{
    if(head == nullptr)
    {
        head = new SNode<T>;
        head->setElement(newElement);
        nr_nodes++;
    }
    else
    {
        SNode<T>* temp = head;
        while(temp->getNext()!= nullptr)
        {
            temp = temp->getNext();

        }
        SNode<T>* temp2= new SNode<T>;
        temp2->setElement(newElement);
        temp->setNext(temp2);
        nr_nodes++;
    }
}
template <typename T>
void SinglyLinkedList<T>::removeBack()
{
    if(head == nullptr)
    {
        return;
    }
    if(head->getNext() == nullptr)
    {
        delete head;
        head = nullptr;
        nr_nodes--;
    }
    else
    {
        SNode<T>* temp = head;
        while(temp->getNext()->getNext() != nullptr)
        {
            temp = temp->getNext();
        }
        delete temp->getNext();
        temp->setNext(nullptr);
        nr_nodes--;
    }
}
template <typename T>
const T& SinglyLinkedList<T>::front() const
{
    return head->getElement();
}
template <typename T>
const T& SinglyLinkedList<T>::back() const
{
    SNode<T>* temp = head;
    while(temp->getNext()!= nullptr)
    {
        temp = temp->getNext();
    }
    return temp->getElement();
}
template <typename T>
void SinglyLinkedList<T>::insert(const T& newElement, int index)
{
    if(index==0)
    {
        addFront(newElement);
    }
    else if (index+1 > nr_nodes)
    {
        throw std::invalid_argument( "bad_index_insert" );
    }
    else
    {
        SNode<T>* temp = head;
        for(int i = 0; i < index - 1; i++)
        {
            temp = temp->getNext();
        }
        SNode<T>* temp2 = temp->getNext();
        SNode<T>* temp_new = new SNode<T>;
        temp_new->setElement(newElement);
        temp_new->setNext(temp2);
        temp->setNext(temp_new);
        nr_nodes++;
    }

}
template <typename T>
void SinglyLinkedList<T>::remove(const T& element)
{
    if(head==nullptr){return;}
    if(head->getNext()==nullptr)
    {
        if(head->getElement()==element){removeFront();}
    }
    else
    {
        SNode<T>* temp = head;
        while( temp->getNext()->getNext() != nullptr )
        {
            if(temp->getNext()->getElement() == element)
            {
                SNode<T>* temp2 = temp->getNext()->getNext();
                delete temp->getNext();
                temp->setNext(temp2);
                nr_nodes--;
            }
            else
            {
                temp = temp->getNext();
            }
        }
        if(temp->getNext()->getElement() == element)
        {
            delete temp->getNext();
            temp->setNext(nullptr);
            nr_nodes--;
        }
        if(head->getElement()==element){removeFront();}
    }
}

template <typename T>
T& SinglyLinkedList<T>::operator[](int index)
{
    if(index+1 > nr_nodes)
    {
        throw std::invalid_argument( "bad_index_out_of_range" );
    }

    SNode<T>* temp = head;
    for(int i =0;i<index;i++)
    {
        temp = temp->getNext();
    }
    return temp->getElement();
}
#endif // DATA_STRUCTURES_SINGLY_LINKED_LIST_HPP
